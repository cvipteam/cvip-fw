# Framework Desarollado por [Colombia Vip](https://www.colombiavip.com) y [Flikimax](http://flikimax.com)

Framework desarrollado tomando lo mejor de diferentes estilos de arquitectura de software, principalmente MVC (model-view-controller).


### ¿Cómo configurar? ###

* Por definir...


### Modo de Uso ###

* Por definir...


### Documentación ###

* Si dos columnas de una misma tabla se llaman igual, Footable solo mostrara la primera que encuentre de izquierda a derecha, Ej: ID | ID. Por lo que se recomienda usar nombre unicos para cada columna.


#### Estructura de archivos que debe tener el plugin que utilizara el framework.

- app
  - assets
    - admin
      - css
      - js
    - css
    - js
  - controllers
    - admin
  - models
  - views
    - admin
- nombre-plugin.php
- index.php


#### Contenido del archivo 'nombre-plugin.php'.

~~~~
<?php
/*
 * Plugin Name: NOMBRE_PLUGIN
 * Plugin URI: URL_PLUGIN
 * Description: DESCRIPCION_PLUGIN
 * Version: 1.0
 * Author: AUTOR__PLUGIN
 * Author URI: AUTOR_URL
 */

 # define('CVIP_DEBUG', true); // VARIABLE PARA DEBUG
 if (class_exists('cvip_framework'))
   $pluginPrueba = new cvip_framework(__FILE__);
~~~~


#### SHORTCODES.

* SE AGREGA UN SHORTCODE (cvip_NOMBRE_PLUGIN) POR CADA PLUGIN QUE USE EL FRAMEWORK.

ARGUMENTOS QUE RECIBE EL SHORTCODE:
~~~~
[cvip_nombrePlugin a='pub' c='nombreControlador' o='nombreMetodo']
Internamente se evalua:
array(
  'a' => 'pub', # ACCESO PUBLICO O PRIVADO.
  'c' => null,  # CONTROLADOR A CARGAR.
  'o' => null,  # OPCION A EJECUTAR.
)
~~~~


#### Links AJAX

Esta es la estructura de como deberia ser el link para el correcto funcionamiento de AJAX.

~~~~
<a href="#" class="cvip-mvc" data-plugin="{NOMBRE_PLUGIN}" data-a="" data-c="CONTROLADOR" data-o="OPCION"  data-container="{NOMBRE_CONTENEDOR}" data-confirmation="{MENSAJE}"> TEXT_LINK </a>
~~~~

* data-plugin: [<NOMBRE_PLUGIN>]
* data-container: [CONTENEDOR]
* data-confirmation: [MENSAJE]
* data-c: [<CONTROLADOR>]
* data-a: [<ADMIN>] Ó [<PUB>]
* data-o: [OPCION]
* class: cvip-mvc
* data-post_action=<1> Ó <0>
* data-back_class="<CLASE_DEL_ELEMENTO_CON_LINK_POST_ACTION>"
* href: #

Si se desea usar un link post acción, use la siguiente estructura separado del contenedor ajax.

~~~~
<a class="{data-back_class}">
  <input type="text" name="" value="TEXT_LINK">
</a>
~~~~

NOTA: La clase del link post acción debe coincidir con el data-back_class.

#### ADMIN TABLE

~~~~
$tabla = $this->adminTable([
  'model'   => {OBJECT},
  'datas'   => {ARRAY}, # TABLE DATA
  'sortableColumns' => {ARRAY},
  'hidden'  => {ARRAY}, # HIDING COLUMNS
  'options' => [
    '{FUNCTION_NAME}' => [
      'label' => '{OPTION_NAME}',
      'args' => ['{PARAMETER_NAME}' => '{PARAMETER_VALUE}'] # ADDITIONAL GET PARAMETERS
    ]
  ],
  'pagination' => 20
]);
~~~~


## Por desarrollar ##

* Rutas publicas.
* Mejoras en el modelo.


## Problemas Actuales ##

* Rutas publicas.
* Adaptación de la funciones 'delete', 'add' y 'save' (del MainController) por los cambios hechos con el 'BelongsTo'.
* Validaciones en la función 'loadController' para verificar que un usuario tiene permisos de controlador y de metodo.

## Solucionado ##


# 20210315

* Avances en la separación de archivos.

# 20210303

* Avances en la separación de archivos.

# 20210301

* Avances en la separación de archivos.

# 20210218

* Avances en la separación de archivos.

# 20210131

* Se agrega autoload.
* Se comienzan a usar Namespace y Use.

# 20210111

* Mejoras en el archivo 'ajaxCMV.js' y en el envío de los datos a php, ahora se recibe cualquier data.

# 20210107

* Se mejora la forma en la que se reciben los datas enviados desde links ajax.

# 20200822

* La clase 'listTable' encargada de las tablas en el admin, ahora pasa a llamarse 'adminTable'.
* En la clase 'listTable' se soluciona el problema de ordenamiento con los ids.
* En la clase 'listTable' en las opciones se mejora y se modifica.
~~~~
# ADMIN TABLE #
$tabla = $this->adminTable([
  'model'   => {OBJECT},
  'datas'   => {ARRAY}, # TABLE DATA
  'sortableColumns' => {ARRAY},
  'hidden'  => {ARRAY}, # HIDING COLUMNS
  'options' => [
    '{FUNCTION_NAME}' => [
      'label' => '{OPTION_NAME}',
      'args' => ['{PARAMETER_NAME}' => '{PARAMETER_VALUE}'] # ADDITIONAL GET PARAMETERS
    ]
  ],
  'pagination' => 20
]);
# ADMIN TABLE #
~~~~

# 20200531

* Se arregla y optimiza la función 'edit' que habia dejado de funcionar por todos los cambios en belongs to y por el manejo del orden de los array en la función 'createForm'.

# 20200528

* Actualización del "README.md".
* Se arregla la función add.

# 20200525

* Se optimiza la clase 'cvipHtml'.

# 20200515

* Corrección en la carga de los css y js.

# 20200511

* Se corrige la función 'textarea' de la clase 'cvipHtml' que no insertaba el campo default.

# 20200508

* Optimizaciones generales.

# 20200504

* Mejoras en el buscador de las tablas admin.
* Se agregar una nueva variable para condiciones adicionales en el buscador.
* Se mejora la función 'search' en el 'mainModel'.

# 20200502

* Se corrige la función para crear formularios que omitia el orden enviado.   

# 20200429

* Corrección de un wp_die que afectaba el frontend que estaba en la clase principal de framework (linea 267).
* Corrección en la función 'optionsM', daba error si no tenia datos, el retorno en dicho caso ahora es null.
* Corrección en los permisos de usuario que afectaba el frontend, evitando que se pudieran ejecutar los shortcodes.

# 20200428

* Las opciones del ADMIN TABLE ahora se pueden enviar como un arrar donde el key pasa a ser la option.
~~~~
$table = $this->adminTable([
  'model'   => OBJECTO_MODELO,
  'datas'    => DATOS,
  'columns'  => [
    'COLUMNA_NAME'    => 'NOMBRE DE LA COLUMNA',
  ],
  'options' => [
    'OPTION_NAME' => ['label' => 'NOMBRE DE LA OPCION', true]
  ],
  'pagination' => 20
]);
~~~~
* optimización en la clase 'listTable'.
* Se corrige el return de la función estatica 'textarea' de la clase 'cvipHtml'.

# 20200411

* Se corrige la función 'getCurrentUserRole' de la Clase 'CvipHelper' que al usar Shortcodes, en determinados casos intentaba validar permisos a usuarios no logueados.
* La opción 'search' para el array de la función 'adminTable' ahora se define en el modelo, quedando como una variable.
~~~~
class MODEL_NAME extends MainModel {
  var $search = 'COLUMN_NAME';
}
~~~~

En caso de querer de que la busqueda se haga teniendo en cuenta diferentes columnas, se agregan como un array;
~~~~
class MODEL_NAME extends MainModel {
  var $search = ['COLUMN_NAME_1', 'COLUMN_NAME_2'];
}
~~~~

# 20200121

* Ajustes a la redirección de inicio de sesión.
* Test de Permisos; ahora funcional.
* Correcciones en el 'mainController' y el 'adminController' sobre el mainModel cuando no exite y se quiere editar o agregar.

# 20191202

* Se actualiza la estructura de archivos que debe tener el plugin que utilizara el framework.
* La propiedad 'listTable' pasa a ser parte de la clase 'html'.

# 20191130

* Ahora se tendra en cuenta la variable definida como 'CVIP_DEBUG' para los mensajes de Debug, ya sea por consola o directamente en pantalla.
* Se modifica la función 'loadModel' para retornar NULL cuando no exita la tabla.
* Se corrige un Bug en la función estatica 'sendToConsole' de la clase 'cvipHtml' que al enviar un type diferente a 'log', 'warn' o 'error', sacaba un mensaje de error.
Ahora se valida y en caso de que el type no exista, se deja por defecto 'log'.
* Se agrega un mensaje en la función 'loadView' en caso de que la vista no exista y que la variable 'CVIP_DEBUG' este definida.
* El mensaje 'Warning: The Main Model no exist.' se pasó a la función 'loadModel'.
* Se verifica que se cargue un style con el nombre de la app.
* Al cargar una vista se busca si existe un css o un js y se carga.

# 20191116

* Se termina el buscador en las tablas.
* Se crea la función 'adminTable' en la clase 'Admin Controller'.
* Se crea la función 'search' en el 'MainController'.

# 20191115

* Se agrega el buscador en las tablas, quedan por corregir bugs.

# 20191113

* En la propiedad 'statusMode' se actualiza el array para que todos comiencen con 'modified_'.

# 20191111

* Se remueve la clase 'panel' en el html de los acordeones de la pagina de ajustes.
* Se crea un 'AJAX SELECT'.  

# 20191104

* Se agregan las mejoras de con respecto a los fields FILES.
* Ajustes a los permisos.

# 20191021

* Se corrigen bugs de la pagina de ajustes.

# 20191018

* La función add para los controladores queda totalmente funcional.

# 20191017

* La función add para los controladores queda de nuevo funcional (no belongs to de momento).

# 20191016

* Se conecta el 'Belongs To' con las nuevas tablas.

# 20191013

* Las opciones de los metodos de la página de ajustes quedan conectados con la función 'loadController'.
* Se agrega una función en el archivo 'usuarios' para el control de las opciones de Wordpress y las Cap.
* Se modifica la función 'sendToConsole' de la clase 'CvipHtml', convirtiendola en static para asi poder llamarla internamente desde la clase 'MainModel'.   

# 20191011

* Se conectan las opciones de la pagina de ajustes con las capacidades de cada usuario.

# 20191010

* Se termina las configuraciones para las opciones de los metodos y controladores a los que puede acceder un usuario.

# 20191008

* Las opciones de configuración de la Página Settings quedan conectadas.

# 20191005

* Avances en las opciones del usuario que permitirian elegir a que metodos puede acceder el usuario.


## 201909026B

* Ahora las opciones de la tabla con Wordpress aparacen y envian correctamente a los metodos editar y eliminar (falta agregar).

## 201909026

* Avances con las tablas de wordpress, se agregan las opciones de editar y elimnar, aunque estan desconectadas.
* Se dinamiza gran parte del codigo necesario para la creacion de las tablas.

## 201909023

* Consultoria.

## 201909022

* Avances en la página de configuración de cada plugin (permisos usuario).

## 201909020

* Avances en las funciones delete y add con respecto a la nueva implementación de Belongs To.

## 201909019

* POSIBLE BUG: El delete (especificamente el link) de los belongsTo es posible que pueda eliminar registros de otras tablas.   

## 201909018

* La función delete queda funcionando tambien para los belongsTo.

## 201909017

* Se cambian los textos de editar y eliminar por iconos representativos.
* Se crea la carpeta 'img' en 'assets'.
* Avances en la función 'delete' para los 'belongsTo'.

## 201909013

* Se vuelven a poner los nombre de los archivos de clase empezando en minusculas, ejemplo: mainController.php
* Ahora en los controladores si esta seteado correctamente 'BelongsTo', Muestra enlaces en la parte inferior para ver la tabla de la relación.
* Editar funciona con los cambios realizados por el 'BelongsTo'.
* Se adelanta en el envio de todos los datas en cuando se usa el link ajax.

## 20190909

* Ahora al Iniciar Sesión redirige al controlador indicado.
* Se agrerga la función 'sendToConsole($mensaje, $tipo)' al nucleo para poder mostrar en consola.

## 20190904

* Se anotan mejoras para desarrollar.

## 20190828

* Se conectan de nuevo las opciones de la página Settings de cada aplicación (Controllers Enabled) y queda funcional.
* Avances con las opciones individuales de los usuarios en la página Settings.

## 20190825B

* Avance en la funcionalidad de permisos por cada usuarios, ideas y Bugs detectados.

## 20190825

* Se inicia la funcionalidad de permisos por cada usuarios en la pagina de configuracion de cada plugin.

## 20190823

* Se agrega la funcionalidad de status Mode al BelongsTo.
~~~~
var $belongsTo = [
  'NOMBRE_TABLA' => [
    'field' => [
      'NOMBRE_COLUMNA' => 'NOMBRE_AS',
    ],
    'localKey'  => 'IDENTIFICADOR_LOCAL',
    'foreignKey' => 'IDENTIFICADOR_EXTERNO',
    'statusMode' => [
        'columnName' => 'NOMBRE_COLUMNA',
        'settings' => [
          'active' => 'VALOR'
        ]
    ]
  ]
];
~~~~
* Se corrige un BUG al agregar nuevos campos con la funcionalidad de Status Mode que no ponia el valor seteado en las configuraciones de status-active.

## 20190819

* Primera versión de Status Mode, en el MainModel.
* Modificaciones a la función 'edit' del adminController.

## 20190818

* La función 'edit' y 'add' ahora pueden recibir los fields como argumentos. edit($args) y add($args).

## 20190805

* Se soluciona el conflicto que tenia la página de configuración con los permisos de cada usuario.
* El metodo edit ahora pre llena los campos.

## 20190804

* La página de configuración de cada plugin queda funcionando.

## 20190803B

* Corrección en la variable '$this->framework->MainModel' MainController.
* Correciones en las funciones 'default', edit y delete.

## 20190803

* Optimización en la función 'createForm' de la clase MainController.
* Mejoras en la función 'formulario' de la clase CvipHtml.

## 20190802

* Se crea una página de configuracón por cada aplicación, falta agregar las opciones de habilitar y deshabilitar los controladores.
* Se crean las funciones 'getbyId', 'getByKey' y se modifica las función 'belongsToFields' en el MainModel.
* Se cambia el nombre del controlador principal (CvipController) a 'MainController'.
* Cambios en la funcion 'createTable'.

## 20190801

* Se agregan cambios por realizar.

## 20190731

* Se crea la clase CvipHelper.
* Correcciones clase CvipHtml.

## 20190730

* Corrección en la función 'createQueryM' de la clase Model.

## 20190729

* Se agrega Belongs To para los campos de la tabla default.

## 20190725

* La 'Admin Page' ahora es modificable por la aplicación creando un controlador admin con el nombre de la aplicacion (NOMBRE_PLUGIN.Controller).

## 20190724

* Primero avances con Belongs To.

Ejemplo Array Belongs To:

~~~~
var $belongsTo = [
  'contratocategorias' => [
    'field' => [
      'name' => 'Categoria'
    ],
    'localKey'  => 'id_contratocategorias',
    'foreignKey' => 'id'
  ],
  'ciudades' => [
    'field' => [
      'Nombre' => 'Ciudad'
    ],
    'localKey' => 'id_ciudades'
  ]
];
~~~~

### 20190719B

* Traducción del modelo principal.
* Se corrigen las secuelas de la traducción del modelo principal.
* Cambios en algunos nombres de clases y funciones.
* Cambios en algunas vistas privadas.
* Las funciones 'loadAdminView' y 'loadPubView' se unen.

### 20190719

* Se adelanta la traducción del modelo.
* Se inicia la construcción de la página de configuación del Framework.

### 20190718

* Se adelanta la traducción del modelo.

### 20190711-B

* Se crea la función loadModels que recibe un array con los modelos que se queires cargar y retorna un array de objetos.
* Mejoras a la función 'ajaxMVC'.
* Mejoras con los primaryKey en la función 'createTable' de la clase 'html'.
* Adaptación de las funciones 'edit', 'delete' y 'add' con la nueva estructura de createForm.
* Se pasa la función 'add' al controlador principal.
Un controlador publico tambien puede guardar registros.
* Se inicia la traducción de la clase 'Model' en un nuevo archivo llamado 'mainModel'.

### 20190711

* Se realizaron comentarios sobre los proximos avances a realizar.

### 20190710

* Mejoras a la Documentación.
* Mejoras al modelo: Se limpia el modeleo de comentarios basura, se optimizan algunas funciones y se mejora la legibilidad.
* Alistamiento para primer versión de producción.


### 20190707

* Se agrega la función loadControllerStyle($style, $access = '') en la clase 'cvip_framework' para cargar archivos css.
* La página principal de cada plugin mostrara solo los link que el usuario tenga permiso de ver.
* Corrección en la función loadController, cuando en un Shortcode era llamado un método que no existía, generaba un error. Ahora en dicho caso, tomara el método por defecto para suplir el método ingresado e inexistente.
* Avances en la Documentación.

### 20190701

* Se agrega la separación de usuarios.

### 20190618

* Conexión AJAX con DB.
* Corrección de la ruta $this->appControllerPath en AJAX.

### 20190617

* Se elimina la función preLoadController() y su contenido se fusiona con loadController.
* Optimización de la función loadController la cual ahora retorna un buffer.
* Ahora loadController permite cargar cualquier controlador.
* La MENU_PAGE ahora permite cargar controladores y ejecutar acciones tanto publicas como privadas dependiendo los parametros GET. Todos esto para facilitar el debug.

### 20190614

* Refactorizacion de load controller, removido argumento $args.
* Mejorado metodo loadController, en la ejecución del admin controller mejor legibilidad.
* Se planetea remover el $main=adminController y cambiarlo por $this->controllerPath y asi evitar mandar tanto argumento.
* Se agrega el default a cleanString($string, $remove="") para cuando solo se quiere remover el .php.

### 20190612

* Ajax Front-end y Back-end separados.
* Optimización de las funciones loadAdminController, cvip_ajax_mvc, loadPubController, shortcode_cvip_mvc, cvip_pub_ajax_mvc y mainPage (encargada de las paginas principales en el Back-end).

### 20190611

* Mejroas y remoción de comentarios.

### 20190607

* Mejoras y optimizaciones en la función cvip_ajax_mvc().
* Ahora los controladores reciben parametos como: $this->framework->parameter

### 20190606

* Se agrega AJAX y Shortcodes.
#### SI SOLO SE DEFINIERA UN SOLO SHORTCODE GENERAL, EXISTIRIAN PROBLEMAS CON LAS RUTAS POR QUE UNA APP SOBREESCRIBIRIA LA RUTA, Y EL SHORTCODE TENDRIA QUE RECIBIR DE ALGUNA MANERA UN DATO PARA CONSTRUIR LA RUTA.
#### PARA MOSTRAR EL AJAX EN EL FRONTEND ESTANDO LOGEADO, DEBE LLEGAR '$POS['a'] = pub', DE LO CONTRARIO SOLO CARGARA CONTROLADORES ADMIN

### 20190531c

* Se agrega filtro COMENTADO para interceptar el query.

### 20190531

* Se revisan opciones de redireccionamiento de los admin screens.

### 20190530

* Se permite tener dos plugins con el mismo nombre de admin controller.

### 20190524

* Se agrega la clase WP_Router para la creación de las rutas publicas.

### 20190523

* Se agrega la clase html para las vistas.
* Se agrega la carga de vistas.
* Se agrega la clase model para la carga de modelos.

### 20190522

* Ahora el framework consta de 3 controladores.
	1. El principal (controller.php) que tiene los metodos loadView y loadModel.
	2. El controlador para la parte administrativa (adminController).
	3. El controlador para la parte que veria un usuario estandar.
* Nos aseguramos que al pasarle el $this de la instancia del framework al controlador administrativo que se llame, no creara un ciclo infinito en algun momento (recursión).

### 20190521

* Se crea el panel principal administrativo con el nombre del plugin que instancia el Framework.
* Se crean los paneles administrativos por cada controlador de la carpeta ~~/app/controller/admin/*.php~~

### 20190519

* Re construcción del Framework.
