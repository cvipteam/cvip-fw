function ajaxMVC (postData, contenedor = 'contenedor'){
  contenedor = '.' + contenedor;

  if (postData.allDatas){
    postData.allDatas = JSON.stringify(postData.allDatas);
  }

  // SI NO ES POSTDATA SE RECORRE Y CREE EL POSTDATA
  if (!(postData instanceof FormData)) {
    var data=new FormData();
    for (var elemento in postData) {
      if (postData.hasOwnProperty(elemento)) {
        data.append(elemento, postData[elemento]);
      }
    }
    postData = data;
  }

  if (!postData.get('o')) postData.append('o', 'default');
  postData.append("action", "cvip_ajax_mvc");

  jQuery.ajax({
    type: "POST",
    url: WP_AJAX.ajaxurl,
    data: postData,
    processData: false,
    contentType: false,
    beforeSend: function(data) {
      jQuery(contenedor).html("<marquee class='marq' bgcolor=silver width=200 height=20 direction=left behavior=alternate>...CARGANDO...</marquee>");
    },
    error: function(jqXHR, textStatus, errorMessage) {
      console.log(textStatus);
      console.log(errorMessage);
    },
    success: function(data) {
      jQuery(contenedor).html(data);
      jQuery('.button-volver').click();
    }
  });
};


// FUNCION PARA LOS FORMS
jQuery(document).on("submit","form.asyn", function(event){
  event.preventDefault();
  var postData = new FormData(this);
  ajaxMVC(postData);
});


// FUNCION PARA EL AJAX DISPARADO POR LA CLASE LINK 'cvip-mvc'
jQuery(document).on("click",".cvip-mvc", function(event){
  event.preventDefault();
  var datas = jQuery(this).data();

  if (datas.confirmation) {
    if (!confirm(datas.confirmation)){ return; }
  }

  ajaxMVC({
    'a': datas.a,
    'c': datas.c,
    'o': datas.o,
    'plugin': datas.plugin,
    'allDatas':datas}, datas.container);
});
