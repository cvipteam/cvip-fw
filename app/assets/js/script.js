// = SELECT AJAX =
jQuery(document).ready(function($) {
  $('.ajax-select').change(ajaxSelChange);
});

function ajaxSelChange(event) {
  var optionUser = jQuery(this).val();
  if (optionUser>0) {
    var datas = jQuery('option:selected', this).data();
    var allDatas = Object.entries(datas);

    if (datas.confirmation) {
      if (!confirm(datas.confirmation)){
        return;
      }
    }

    ajaxMVC({
      'a': datas.a,
      'c': datas.c,
      'o': datas.o,
      'plugin': jQuery(this).attr('data-plugin'),
      allDatas
      },
        datas.container
    );
  }
  else {
    // TODO: CAMBIAR A INGLES
    var container = jQuery('option:selected', this).attr('data-container');
    container.html("<pre>USA EL DESPLEGABLE DE ARRIBA</pre>");
  }
}
// = SELECT AJAX =

// = USER PERMISSIONS -SELECT- =
jQuery(document).ready(function($) {
  $('.select-user').change(selChange);
});

function selChange(event) {
  console.log("llega");
  var optionUser = jQuery(this).val();
  if (optionUser>0) {
    var user = jQuery('option:selected', this).attr('data-user');
    var pluginName = jQuery(this).attr('data-plugin');
    var url = location.origin + window.location.pathname +'?page=cvip-' + pluginName + '&option=users&user=' + user;
    location.href = url;
  }
  else {
    // TODO: CAMBIAR A INGLES
    jQuery('.container').html("<pre>USA EL DESPLEGABLE DE ARRIBA</pre>");
  }
}
// = USER PERMISSIONS -SELECT- =




// = SETTINGS PAGE (ACCORDION) =
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function(event) {
    event.preventDefault();
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
// = SETTINGS PAGE (ACCORDION) =
