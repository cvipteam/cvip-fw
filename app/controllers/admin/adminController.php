<?php
namespace app\controllers\admin;
use app\controllers\mainController;


class adminController extends mainController {
  // TODO: crear construc que herede el del padre y que le agregue validacion de permisos de usuarioq ue se llame igual al controllador cargado

  # = OPCION POR DEFECTO =
  # = LISTAR DEL ADMIN ES DIFERENTE AL LISTAR PUBLICO =
  public function default(){
    if (isset($this->framework->mainModel->wpdb)){
      $hidden = [$this->framework->mainModel->primaryKey];
      if ($this->framework->mainModel->statusMode){
        foreach ($this->framework->mainModel->statusMode as $value) {
          $statusMode[] = $value['columnName'];
        }
        $hidden = array_merge($hidden, $statusMode);
      }

      # ADMIN ABLE #
      $table = $this->adminTable([
        'model'   => $this->framework->mainModel,
        'datas'   => $this->framework->mainModel->DB('ARRAY_A'),
        # 'hidden'  => [FIELD_NAME],  // IDEA: QUE SEA UNA OPCION DEL MODELO Y VALIDAR SI EXISTE ACA?
        'options' => [
          'add'    => ['label' => 'Add', true],
          'edit'   => ['label' => 'Editar', true],
          'delete' => ['label' => 'Delete', true]
        ],
        'pagination' => 20
      ]);
      # ADMIN TABLE #

      # BELONGS TO
      if ($this->framework->mainModel->belongsTo){
        foreach ($this->framework->mainModel->belongsTo as $tableName => $args) {
          $belongsTo[$tableName] = array_values($args['field'])[0];
        }
      }
      # BELONGS TO
    }

    echo $this->loadView('default', [
      'tableName'      => (isset($_GET['tableName'])) ? "&tableName={$_GET['tableName']}" : '',
      'table'          => @$table,
      'belongsTo'      => (isset($belongsTo)) ? $belongsTo : array()
    ], 'admin');
  }

  # = RECIBE LOS ARGS PARA LA CREACION DE LA TABLA ADMIN Y RETORNA LA VISTA CON LA TABLA =
  public function adminTable($args){
    $this->framework->cvipHtml->adminTable->prepare_items($args);
    return $this->loadView('adminTable', ['search' => (isset($args['model']->search)) ? true : false], 'admin');
  }

  # = FUNCION PARA EDITAR UN REGISTRO =
  public function edit($args = null){
    if ($this->framework->parameter){
      // TODO: VALIDAR PERMISOS.
      if (isset($this->framework->parameter['model'])){
        if ($this->framework->modelsBelongsTo[$this->framework->parameter['model']]){
          $model = $this->framework->modelsBelongsTo[$this->framework->parameter['model']];
        } else if ($this->framework->mainModel->tableName == $this->framework->parameter['model']) {
          $model = $this->framework->mainModel;
        } else {
          if (defined('CVIP_DEBUG')) cvipHtml::sendToConsole("Warning: The Model {$this->framework->parameter['model']} no exist.", 'warn');
          wp_die("Error al insertar los datos.");
        }
        unset($this->framework->parameter['model']);
      }
      $model->update(
        $this->framework->parameter, [
          $model->primaryKey => $this->framework->parameter[$model->primaryKey]
        ]
      );
    } else {
      if (isset($args['model'])){
        $model = $this->loadModel($args['model']);
      } else if (isset($_GET['tableName']) && isset($this->framework->modelsBelongsTo[$_GET['tableName']])) {
        $model = $this->framework->modelsBelongsTo[$_GET['tableName']];
      } else {
        $model = $this->framework->mainModel;
      }

      if (isset($args['id']) && !empty($args['id'])){
        $currentRegistry = $args['id'];
      } else if (isset($_GET[$model->primaryKey])) {
        $currentRegistry = $_GET[$model->primaryKey];
      } else {
        return $this->default();
      }
      $currentRegistry = $model->getByKey($currentRegistry, 'ARRAY_A');

      if ($currentRegistry) {
        foreach ($currentRegistry[0] as $key => $value)
          $defaultFields[$key] = [ 'default' => $value ];

        $defaultFields[$model->primaryKey] = array_merge($defaultFields[$model->primaryKey], [
            'type' => 'hidden',
            'label' => '',
        ]);

        if (isset($args['fields']) && is_array($args['fields']))
          $defaultFields = array_replace_recursive($defaultFields, $args['fields']);

        if ($model->statusMode){
          foreach ($model->statusMode as $value) {
            $statusMode[$value['columnName']] = [
              'type'  => 'hidden',
              'label' => ''
            ];
          }
          $defaultFields = array_replace_recursive($defaultFields, $statusMode);
        }

        $datas = $this->createForm([
          'model'       => $model->tableName,
          'option'      => __FUNCTION__,
          'a'           => 'admin',
          'fields'      => array_replace_recursive($model->fields, $defaultFields),
          'submit'      => 'Save'
        ]);

        echo $this->loadView('edit', $datas, 'admin');
      } else {
        return $this->default();
      }
    }
  }

  # = FUNCION PARA ELIMINAR UN REGISTRO =
  public function delete(){
    if ($this->framework->parameter){
      if ( $this->framework->cvipHelper->permissions( [$this->framework->cvipHelper->cleanString($this->framework->controllerName, 'Controller')] ) ){
        if ( isset($this->framework->parameter['tablename']) ){
          if (isset($this->framework->modelsBelongsTo[$this->framework->parameter['tablename']])){
            $model = $this->framework->modelsBelongsTo[$this->framework->parameter['tablename']];
          }else if ($this->framework->mainModel->tableName == $this->framework->parameter['tablename']){
            $model = $this->framework->mainModel;
          }else {
            die('Sorry, You Are Not Allowed to Access This Page.');
          }
          $model->delete($this->framework->parameter['id']);
        }else {
          die('Sorry, You Are Not Allowed to Access This Page.');
        }
      }
    }else {
      if ((isset($_GET['tableName']) && isset($this->framework->modelsBelongsTo)) && $this->framework->modelsBelongsTo[$_GET['tableName']]->tableName == $_GET['tableName']){
        $model = $this->framework->modelsBelongsTo[$_GET['tableName']];
      }else {
        $model = $this->framework->mainModel;
      }
      $info = $model->getByField($_GET[$model->primaryKey], $model->primaryKey);
      echo $this->loadView('delete', [
          'info'            => $info[0],
          'appName'         => $this->framework->appName,
          'primaryKey' => $info[0]->{$model->primaryKey},
          'tableName'       => $model->tableName
      ], 'admin');
    }
  }

  # = PAGINA PRINCIPAL DE LA APLICACION =
  public function mainPage(){
    $adminControllers = [];
    if ($this->framework->controllersEnabled)
      foreach ($this->framework->controllersEnabled as $controllerEnabled) {
        if ( ($this->framework->cvipHelper->getCurrentUserRole() == 'administrator' || $this->framework->cvipHelper->getCurrentUserRole() == strtoupper($this->framework->appName)) || (get_option("cvip-user-enabled-".$this->framework->cvipHelper->getCurrentUserRole()."-$controllerEnabled")) )
          $adminControllers[] = $controllerEnabled;
        }
    echo $this->loadView('mainPage', $adminControllers, 'admin');
  }

  # ==============================
  # ============ BETA	============
  # ==============================

  // IDEA: Debemos mejorar el nombre?
  public function belongsTo(){
    if (isset($_GET['tableName']) && $_GET['tableName']) {
      $tableName = $_GET['tableName'];
      if ($this->framework->mainModel->belongsTo && array_key_exists($tableName, $this->framework->mainModel->belongsTo)) {
        echo $this->framework->loadController([
          'a' => 'admin',
          'c' => $tableName,
          'o' => 'default'
        ]);
      }else {
        $this->default();
      }
    }else {
      $this->default();
    }
  }

  # ==============================
  # ============ BETA	============
  # ==============================

}
