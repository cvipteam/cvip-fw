<?php
namespace app\controllers;
use app\models\mainModel as mainModel;
use helpers\files as files;

class mainController {
  public $framework;

  function __construct($framework = null){
    $this->framework = $framework; // TODO: QUITAR $this->framework, DEJAR $this = $framework;

    $this->framework->controllerName = ($this->framework->controllerName) ? $this->framework->controllerName : get_called_class();
    $this->framework->mainModel = $this->framework->loadAssets->loadModel($this->framework->controllerName);

    if ($this->framework->mainModel->belongsTo) {
      $this->framework->modelsBelongsTo = $this->framework->loadAssets->loadModels(array_keys($this->framework->mainModel->belongsTo));
    }

    $this->framework->files = new files([
      'model' => (isset($_REQUEST['model'])) ? $this->framework->loadAssets->loadModel($_REQUEST['model']) : $this->framework->mainModel,
      'helper' => $this->framework->cvipHelper,
    ]);
  }

  # = FUNCION PARA AGREGAR UN REGISTRO =
  public function add($args = null){
    if (!$this->framework->mainModel){
       echo $this->loadView('default', null, 'admin');
       return;
    }
    if ($this->framework->parameter){
      if (isset($this->framework->parameter['model'])){
        if ($this->framework->modelsBelongsTo[$this->framework->parameter['model']]){
          $model = $this->framework->modelsBelongsTo[$this->framework->parameter['model']];
        } else if ($this->framework->mainModel->tableName == $this->framework->parameter['model']) {
          $model = $this->framework->mainModel;
        } else {
          if (defined('CVIP_DEBUG')) $this->framework->cvipHtml->sendToConsole("Warning: The Model {$this->framework->parameter['model']} no exist.", 'warn');
          wp_die("Error al insertar los datos.");
        }
        unset($this->framework->parameter['model']);
      }

      // TODO: EN DESAROLLO: FUNCION PARA VALIDAR SI EL USUARIO TIENE PERMISOS PARA REALIZAR ESTA ACCION
      $this->valModelsBelongsTo($model->tableName);
      $model->save($this->framework->parameter);
      die;
    }else {
      $model = ((isset($_GET['tableName']) && isset($this->framework->modelsBelongsTo)) && $this->framework->modelsBelongsTo[$_GET['tableName']]->tableName == $_GET['tableName']) ? $this->framework->modelsBelongsTo[$_GET['tableName']] : $this->framework->mainModel;

      $defaultFields = array_replace_recursive($model->fields, [
        $model->primaryKey => [
          'type' => 'hidden',
          'label' => '',
          ]
      ]);

      if ($args['fields'] && is_array($args['fields']))
        $defaultFields = array_replace_recursive($defaultFields, $args['fields']);

      if ($model->statusMode){
        foreach ($model->statusMode as $value) {
          $statusMode[$value['columnName']] = [
            'type'  => 'hidden',
            'label' => ''
          ];
        }
        $defaultFields = array_replace_recursive($defaultFields, $statusMode);
      }

      $datas = $this->createForm([
        'model'       => $model->tableName,
        'option'      => (isset($args['option'])) ? $args['option'] : __FUNCTION__,
        'a'           => 'admin',
        'fields'      => $defaultFields,
        'submit'      => 'Save'
      ]);
      echo $this->loadView('add', $datas, 'admin');
    }
  }




  public function createForm($args) {
    $model = (isset($args['model']) && $args['model'] != $this->framework->mainModel->tableName) ? $this->loadModel($args['model']) : $this->framework->mainModel;
    $fields = ( isset($args['fields']) ) ? $args['fields'] : $model->fields;
    $option = ( isset($args['fields']['option']) && !is_array($args['fields']['option']) ) ? $option = $args['fields']['option'] : (( isset($args['option']) && !is_array($args['option']) ) ? $option = $args['option'] : 'save');

    $defaultFields = [
      'plugin' => [
        'type'    =>"hidden",
        'default' =>$this->framework->appName,
        'required'=>TRUE,
      ],
      'a' => [
        'type'    => "hidden",
        'default' => (isset($args['a']) && !is_array($args['a'])) ? $args['a'] : 'pub',
        'required'=> TRUE,
      ],
      'c' => [
        'type'    => "hidden",
        'default' => (isset($args['controller'])) ? $args['controller'] : $this->framework->cvipHelper->cleanString($this->framework->controllerName, 'Controller'),
        'required'=> TRUE,
      ],
      'o' => [
        'type'    => "hidden",
        'default' => $option,
        'required'=> TRUE,
      ],
      'model' => [
        'type'    => "hidden",
        'default' => $model->tableName,
        'required'=> TRUE,
      ],
      'className' => (isset($args['className'])) ? $args['className'] : null,
      'submit'    => (isset($args['submit'])) ? $args['submit'] : null
    ];
    return $this->framework->cvipHtml->form(array_merge($fields, $defaultFields));
  }


  public function save(){
    // unset($this->framework->parameter['currentModel']);
    $this->framework->mainModel->save($this->framework->parameter);
  }

  # CARGA Y RETORNA UNA VISTA
  public function loadView($view, $datas = null, $access = ''){
    $access = ($access == 'admin') ? 'admin/' : '';
    $path = "/app/views/{$access}{$view}.php";
    ob_start();
      if (file_exists($this->framework->paths->appPath.$path)){ # VIEWS APP
        require ($this->framework->paths->appPath.$path);
      } else if (file_exists(CVIP_FW_PATH.$path)){ # VIEWS FW
        require (CVIP_FW_PATH.$path);
      } else {
        if (defined('CVIP_DEBUG')) $this->framework->cvipHtml->sendToConsole("Error: The View $view no exist.", 'error');
      }
    return ob_get_clean();
  }


}
