<?php
namespace app\controllers;
use app\controllers\mainController;

class pubController extends mainController {

  function default(){
    if (isset($this->framework->mainModel->wpdb)){
      $view = $this->framework->cvipHtml->createTable([
        "tableName"    => $this->framework->mainModel->tableName,
        "datos"        => $this->framework->mainModel->DB(),
        "buscador"     => true,
        "tamano"       => '10',
        "modificacion" => true
      ]);
      echo $this->loadView('list', $view);
    }
  }

}
