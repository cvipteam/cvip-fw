<?php
namespace app\models;

class mainModel {
  public $tableName;
  public $query;
  public $wpdb;

  public $search = null;
  public $fields = array();
  public $belongsTo = null;
  public $primaryKey = null;
  public $statusMode = null;

  # = POR IMPLEMENTAR =
  public $hasMany = null;
  # = POR IMPLEMENTAR =

  function __construct($tableName = NULL){
    $this->tableName = ($tableName) ? $tableName : self::tableName();
    if ($this->tableName = $this->tableExists($this->tableName)){
      global $wpdb;
      $this->wpdb = $wpdb;
      $this->primaryKey = $this->getPrimaryKey($this->tableName);
      $this->statusMode = $this->statusMode();
      $this->query = $this->createQueryM();
      $this->fields = $this->createFormfields();
    }
  }

  # = STATUS MODE =
  public function statusMode(){
    if ($this->statusMode){
      $colomns = $this->columnNamesM();
      foreach (array_keys($this->statusMode) as $value) {
        if (isset($this->statusMode[$value]['columnName']) && !in_array($this->statusMode[$value]['columnName'], $colomns))
          return;
      }
      return array_replace_recursive([
        'modified_status' => [
          'columnName' => 'modified_status',
          'settings' => [
            'active' => 'active',
            'update'   => 'edited',
            'delete' => 'deleted'
          ]
        ],
        'modified_date' => [
          'columnName' => 'modified_date'
        ],
        'modified_by' => [
          'columnName' => 'modified_by'
        ],
        'modified_parent' => [
          'columnName' => 'modified_parent',
          'settings' => [
            'key' => $this->primaryKey,
          ]
        ]
      ], $this->statusMode);
    }
    return;
  }

  # = BUSCA Y RETORNA EL KEY PRIMARY DE LA TABLA =
  public function getPrimaryKey($table = null){
    $table = ($table) ? $table : $this->tableName;
    $query = "SELECT * FROM information_schema.KEY_COLUMN_USAGE WHERE KEY_COLUMN_USAGE.TABLE_NAME  = '{$table}'";
    $result = $this->wpdb->get_results($query, 'OBJECT');
    if (isset($result[0]->COLUMN_NAME))
      return $result[0]->COLUMN_NAME;
  }

    # = EJECUTA $THIS->QUERY Y RETORNA SU RESULTADO =
    function DB($type = "OBJECT"){
      $result = $this->wpdb->get_results($this->query, $type);
      return ($result) ? $result: null;
    }

    # = RETORNA UN ARRAY DE OPCIONES LISTAS PARA USAR EN UN SELECT =
    function options(){
      foreach ($this->DB("ARRAY_N") as $option) {
        $options[$option[0]] = $option[1];
      }
      return $options;
    }
    function optionsM($query = null){
      $datas = ($query) ? $this->wpdb->get_results($query, 'ARRAY_N') : $this->DB("ARRAY_N");
      $options = null;
      foreach ($datas as $data) {
        $options[$data[0]] = $data[1];
      }
      return $options;
    }

    # = CREA UN ARRAY CON LAS COLUMNAS DE LA TABLA Y QUEDA LISTO PARA CRAER FORMULARIOS =
    public function createFormfields(){
      $values = [];
      $disabled = null;
      foreach ($this->info() as $key) {
        array_push($values, [
          'label'    => $key['Field'],
          'type'     => 'text',
          // 'type'     => ($key['Field'] == $this->primaryKey) ? 'hidden' : 'text',
          'disabled' => $disabled,
          'required' => ($key['Null'] == 'YES') ? FALSE: TRUE,
          'as'       => $key['Field']
        ]);
      }
      $fields = ($this->fields) ? array_replace_recursive(array_combine($this->columnNamesM(), $values), $this->fields) : array_combine($this->columnNamesM(), $values);
      return (isset($this->belongsTo)) ? array_replace_recursive($fields, $this->belongsToFields()) : $fields;
    }

    public function belongsToFields(){
      # = Este ciclo se encarga de generar el array de selección para el campo de belongsTo
      foreach ($this->belongsTo as $table => $args) {
        $field = array_keys($args['field']);
        $fieldKey = array_values($args['field']);
        $infoField = $this->info($table, $field[0])[0];

        // TODO: UNIR
        $localKey = isset($args['localKey']) ? $args['localKey']: "id_$table";
        $foreignKey = isset($args['foreignKey']) ? $args['foreignKey'] : $this->getPrimaryKey($table);
        $this->belongsTo[$table]['localKey'] = $localKey;
        $this->belongsTo[$table]['foreignKey'] = $foreignKey;

        $query = "SELECT
          $table.$foreignKey,
          $table.$field[0]
          FROM $this->tableName
          RIGHT JOIN $table ON $table.$foreignKey = $this->tableName.$localKey";

        if (isset($args['statusMode']['columnName']) && isset($args['statusMode']['settings']['active']))
          $query .= " WHERE {$table}.{$args['statusMode']['columnName']} = '{$args['statusMode']['settings']['active']}'";

        $belongsToFields[$localKey] = [
          'label'       => $fieldKey[0],
          'placeholder' => (isset($args['placeholder']) && !empty($args['placeholder'])) ? $args['placeholder'] : 'Select',
          'default'     => (isset($args['field']['default']) && !empty($args['field']['default'])) ? $args['field']['default'] : null,
          'type'        => 'select',
          'options'     => $this->optionsM($query),
          'required'    => ($infoField['Null'] == 'YES') ? FALSE: TRUE,
        ];
      }
      return $belongsToFields;
    }

    # RETORNA EL NOMBRE DE LA CLASE DEL MODELO QUE EXTIENDE A MAINMODEL
    public static function tableName() {
      return strtolower(get_called_class());
    }

    # = RETORNA UN ARRAY CON EL NOMBRE DE LAS COLUMNAS DE LA TABLA =
    public static function columnNames(){
      global $wpdb;
      $query="SELECT
            column_name
          FROM information_schema.columns
          WHERE table_name = '".self::tableName()."';";

      return $wpdb->get_col($query);
    }
    public function columnNamesM(){
      $query = "SELECT
          column_name
        FROM information_schema.columns
        WHERE table_name = '$this->tableName'";

      return $this->wpdb->get_col($query);
    }
    # BETA #
    public function getPrepareColumns($tableName = null){
      $tableName = ($tableName) ? $tableName : $this->tableName;
      foreach ($this->columnNamesM($tableName) as $columnName) {
        $columns[$columnName] = $this->fields[$columnName]['as'];
      }
      if (isset($this->belongsTo)){
        foreach ($this->belongsTo as $table => $args) {
          unset($columns[$args['localKey']]);
          $merge[array_values($args['field'])[0]] = array_values($args['field'])[0];
        }
        $columns = array_replace_recursive($columns, $merge);
      }
      return $columns;
    }
    public function getPrepareHiddenColumns($tableName = null){
      $tableName = ($tableName) ? $tableName : $this->tableName;
      $columns = array();
      foreach ($this->columnNamesM($tableName) as $columnName) {
        if ($this->fields[$columnName]['type'] == 'hidden')
          $columns[$columnName] = $this->fields[$columnName]['as'];
      }
      return $columns;
    }
    # BETA #

    # = CONVIERTE UN ARRAY DE COLUMNAS A UN STRING PARA SQL =
    public static function columnString ( $columns ) {
      if ( empty( $columns ) )
        return '*';
      if ( is_string( $columns ) )
        return $columns;
      if ( is_array( $columns ) )
        return implode( ', ', $columns );
    }

    # = RETORNA EL VALOR DEL ULTIMO ID (NO USAR EN TABLAS SIN ID) =
    public static function lastId() {
      global $wpdb;
      $query='SELECT MAX(id) FROM '.self::tableName();
      return $wpdb->get_var($query);
    }
    public function lastIdM() {
      $query = "SELECT MAX(id) FROM $this->tableName";
      return $this->wpdb->get_var($query);
    }

    # = RETORNA EL ULTIMO VALOR DEL CAMPO QUE SE LE PASE, SI NO LE LLEGA $FIELD ENTONCES USA EL PRIMARY KEY =
    public function lastPrimaryKey() {
      $query = "SELECT MAX($this->primaryKey) FROM $this->tableName";
      return $this->wpdb->get_var($query);
    }

    # = RECIBE UN NOMBRE DE COLUMNA Y RETORNA SU VALOR BASADO EN EL ULTIMO ID (NO USAR EN TABLAS SIN ID) =
    public static function lastColumn($columna) {
      global $wpdb;
      $query="SELECT $columna FROM ".self::nombreTabla()." WHERE id = ".self::ultimoId();
      return $wpdb->get_var($query);
    }
    public function lastColumnM($column) {
      $query = "SELECT $column FROM $this->tableName WHERE id = {$this->lastPrimaryKey()}";
      return $this->wpdb->get_var($query);
    }

    //Recibe un array de columnas y devuelve el valor de la ultima fila dentro de un array (No usar en tablas sin ID)
    # = RECIBE UN ARRAY DE COLUMNAS Y RETORNA EL VALOR DE LA ULTIMA FILA DENTRO DE UN ARRAY (NO USAR EN TABLAS SIN PRIMARY KEY) =
    public static function last($columns="*") {
      global $wpdb;
      $columns=self::columnString($columns);
      $query="SELECT $columns FROM ".self::tableName()." WHERE id = ".self::lastId();
      return $wpdb->get_row($query, "ARRAY_A");
    }
    public function lastM($columns="*") {
      $columns = $this->columnString($columns);
      $query = "SELECT $columns FROM $this->tableName WHERE $this->primaryKey = {$this->lastPrimaryKey()}";
      return $this->wpdb->get_row($query, "ARRAY_A");
    }

    # = DEVUELVE UN ARRAY CON LOS NOMBRES DE LAS COLUMNAS DE LA TABLA Y TODOS LOS VALORES EN NULL =
    public function describe(){
      foreach ($this->columnNamesM() as $key => $value) {
        $fillNull[$value] = null;
      }
      return $fillNull;
    }

    # = CUENTA LOS REGISTROS QUE CUMPLEN CON UNA CONDICIÓN
    function count($where = NULL){
      $query = "SELECT COUNT(*) FROM $this->tableName";
      if ($where){
        $query .= ' WHERE '; $query .= (is_array( $where )) ? implode(' AND ', $where ) : $where;
      }


      return $this->wpdb->get_var($query);
    }

    # = MEZCLA EL ARRAY RECIBIDO MÁS EL QUE DESCRIBE LA TABLA PARA LLENAR DE NULLS LAS COLUMNAS VACIAS  =
    function build($row){
      return array_merge($this->describe(), $row);
    }

    # ===============================
    # =             CRUD            =
    # ===============================
    public function prepareStatusMode($row, $status, $parent = null){
      $row[$this->primaryKey] = $this->lastPrimaryKey()+1;
      $row[$this->statusMode['modified_status']['columnName']] = $status;
      $row[$this->statusMode['modified_date']['columnName']] = cvipHelper::fecha();
      $row[$this->statusMode['modified_by']['columnName']] = get_current_user_id();
      $row[$this->statusMode['modified_parent']['columnName']] =  ($parent) ? $parent : 0;
      return $row;
    }

    # = GUARDA EL REGISTRO CONSTRUYENDO LA FILA Y SALVANDOLA EN BASE DE DATOS =
    function save($row){
      $row = $this->build($row);
      if ($this->statusMode){
        $status = ($row[$this->statusMode['modified_status']['columnName']]) ? $row[$this->statusMode['modified_status']['columnName']] : $this->statusMode['modified_status']['settings']['active'];
        $parent = ($row[$this->statusMode['modified_parent']['columnName']]) ? $row[$this->statusMode['modified_parent']['columnName']] : 0;
        $row = $this->prepareStatusMode($row, $status, $parent);
      }
      return $this->wpdb->insert($this->tableName, $row);
    }

    # = RECORRE EL ARRAY RECIBIDO Y GUARDA CADA FILA =
    function saveMany($rows){
      foreach ($rows as $key => $row) {
        $result = $this->save($row);
        if (!$result) {
          return $result; // SI HAY ERROR DETIENE EL CICLO
        }
      }
      return $result;
    }

    # = ACTUALIZA UN REGISTRO DONDE SE DE UNA CONDICIÓN =
    function update($row, $where){
      if ($this->statusMode){
        $newRow = $this->getByKey($row[$this->primaryKey], 'ARRAY_A')[0];
        $newRow[$this->statusMode['modified_parent']['columnName']] = $row[$this->primaryKey];
        $newRow[$this->statusMode['modified_status']['columnName']] = $this->statusMode['modified_status']['settings'][__FUNCTION__];
        $newRegistry = $this->save($newRow);
      }
      $result = $this->wpdb->update($this->tableName, $row, $where);
      return $result;
    }

    # = BORRA UN REGISTRO CON UN ID ESPECIFICO =
    function delete ($id, $column = null){
      if ($this->statusMode){
        $currentRegistry = $this->getByKey($id, 'ARRAY_A');
        $currentRegistry[0][$this->statusMode['modified_status']['columnName']] = $this->statusMode['modified_status']['settings']['delete'];
        return $this->update($currentRegistry[0], [$this->primaryKey => $id]);
      }
      $column = ($column) ? $column : $this->primaryKey;
      $result = $this->wpdb->delete($this->tableName, array($column => $id));
      return $result;
    }
    # ===============================
    # =             CRUD            =
    # ===============================

    # = FUNCION QUE RETORNA UN QUERY RECIBIENDO UNOS PARAMETROS =
    public function createQuery($columns = null, $where = null, $order = null, $limit = null){
      $query="SELECT {$this->columnString($columns)} FROM $this->tableName";
      if ( $where )
        $query .= ' WHERE ' . ( is_array( $where ) ? implode( ' AND ', $where ) : $where );
      if ( $order )
        $query .= ' ORDER BY ' . ( is_array( $order ) ? implode( ', ', $order ) : $order );
      if ( $limit )
        $query .= ' LIMIT ' . $limit;
      return $query;
    }
    public function createQueryM($columns = null, $where = null, $order = null, $limit = null){
      if ($this->belongsTo){
        foreach ($this->columnNamesM() as $key => $value) {
          $columns[] = "$this->tableName.$value";
        }

        foreach ($this->belongsTo as $table => $args) {
          $localKey = isset($args['localKey']) ? $args['localKey'] : "id_$table";
          $foreignKey = isset($args['foreignKey']) ? $args['foreignKey'] : $this->getPrimaryKey($table);
          $belongsTo[] = "LEFT JOIN $table ON $table.$foreignKey = $this->tableName.$localKey";
          // TODO: AL CAMBIAR EL CAMPOS $args['field'] POR UN ARRAY (MODIFICAR)
          $fieldValue = array_values($args['field']);
          $fieldKey = array_keys($args['field']);
          $columnsBelongsTo[] = "$table.$foreignKey AS '{$fieldValue[0]}'";
          $columns[array_search("{$this->tableName}.$localKey", $columns)] = "$table.{$fieldKey[0]} AS '{$fieldValue[0]}'";
        }
        $columns = implode(', ',$columns);
        $belongsTo = implode(' ',$belongsTo);
      }

      $columns = ($columns) ? $columns : '*';
      $query = "SELECT ".$columns." FROM $this->tableName ".@($belongsTo)??'';
      if ($this->statusMode){
        if ($where){
          $where = is_array($where) ? array_push($where, "{$this->statusMode['modified_status']['columnName']} = '{$this->statusMode['modified_status']['settings']['active']}'") : "$where AND {$this->statusMode['modified_status']['columnName']} = '{$this->statusMode['modified_status']['settings']['active']}'";
        }else {
          $where = "{$this->tableName}.{$this->statusMode['modified_status']['columnName']} = '{$this->statusMode['modified_status']['settings']['active']}'";
        }
      }

      if ( $where )
        $query .= ' WHERE ' . ( is_array( $where ) ? implode( ' AND ', $where ) : $where );
      if ( $order )
        $query .= ' ORDER BY ' . ( is_array( $order ) ? implode( ', ', $order ) : $order );
      if ( $limit )
        $query .= ' LIMIT ' . $limit;

      return $query;
    }

    # = FUNCION PARA BUSCAR USADA POR EL FORM DE LAS TABLAS =
    public function search($fields, $search, $searchWhere = null, $type = 'ARRAY_A'){
      if (!$fields || !$search || !is_string($search))
        return false;

      if (is_array($fields)){
        foreach ($fields as $key => $field) {
          if ($this->belongsTo){
            foreach ($this->belongsTo as $table => $args) {
              if (in_array($field, $args)){
                $fields[$key] = "{$table}.nombre LIKE '%{$search}%'";
                continue 2;
              }
            }
          }
          $fields[$key] = "{$this->tableName}.$field LIKE '%{$search}%'";
        }
        $fields = implode(" OR ", $fields);
      } else if (is_string($fields)){
        $fields = "{$this->tableName}.{$fields} LIKE '%{$search}%'";
      } else {
        return false;
      }
      // TODO: HACER COMO STRING Y ARRAY
      if ($searchWhere)
        $fields = " $searchWhere AND $fields ";

      $result = $this->wpdb->get_results($this->createQueryM(null, $fields), $type);
      return ($result) ? $result: null;
    }

    # = SELECCIONA CON UN ARRAY DE COLUMNAS Y UN ARRAY DE CONDICIONES =
    function select($columns = null, $where = null, $order = null, $limit = null){
      $query = $this->createQuery($columns ,$where, $order, $limit);
      return $this->wpdb->get_results($query);
    }

    # = RECIBE UN VALOR Y EL NOMBRE DE UNA COLUMNA. SI NO LLEGA $campo TOMA EL KEY PRIMARY, RETORNA UN REGISTRO =
    public function getByField($value, $campo = null, $type = 'OBJECT'){
      if (!$campo)
        $campo = $this->getPrimaryKey();
      $query = "SELECT * FROM $this->tableName WHERE $campo = $value";
      $result = $this->wpdb->get_results($query, $type);
      return $result;
    }

    # = RETORNA UN REGISTRO SI COINCIDE EL VALOR =
    public function getById($value, $type = 'OBJECT'){
      return $this->getByField($value, 'id', $type);
    }

    # = RETORNA UN REGISTRO SI COINCIDE CON EL PRIMARY KEY =
    public function getByKey($value, $type = 'OBJECT'){
      return $this->getByField($value, $this->getPrimaryKey(), $type);
    }

    # =====================
    # = METODOS EN PRUEBA =
    # =====================

/*
  // FUNCCION HAS MANY
  function hasMany ($tabla = NULL, $datos, $id, $order = NULL, $limit = NULL){

    $tabla = $tabla??$this->nombreTabla;

    if ($datos['campos']){
      $columnas = $this->converKeyAsCampos($datos['campos'], $tabla);
      $columnas = implode( ', ', $columnas );
    }

    $query = "SELECT $columnas FROM $tabla WHERE $tabla.".$datos['keyExterna']."= $id";

    //if ( $where || !empty( $stringLeftJoins ) )
    // $query .= ' WHERE ' . ( is_array( $where ) ? implode( ' AND ', $where ) : $where);
    if ( $order )
      $query .= ' ORDER BY ' . ( is_array( $order ) ? implode( ', ', $order ) : $order );
    if ( $limit )
      $query .= ' LIMIT ' . $limit;

    $result = $this->wpdb->get_results($query, 'ARRAY_A');
    return $result;
  }
*/

    # = RETORNA INFORMACION DE LOS INDICES DE UNA TABLA =
    public function tableIndexes($table){
      $query = "SHOW INDEX FROM $table";
      return $this->wpdb->get_results($query);
    }

    # = RETORNA LOS NOMBRES DE LAS TABLAS DE UNA BASE DE DATOS =
    public static function showFullTables($type="OBJECT"){
      global $wpdb;
      $query = "SHOW FULL TABLES FROM ".DB_NAME;
      return $wpdb->get_results($query, $type);
    }

    # = COMPRUEBA SI LA TABLA QUE RECIBE EXISTE Y SI ES EL CASO, RETORNA EL NOMBRE DE LA TABLA =
    public static function tableExists($table){
      global $wpdb;
      $query = "SHOW TABLES LIKE '$table'";
      $result = $wpdb->get_results($query, 'ARRAY_N');
      if ( isset($result[0][0]) && $result[0][0] == $table )
        return $result[0][0];
      return null;
    }

    # = RETORNA INFORMACIÓN SOBRE LA ESTRUCTURA DE LA TABLA =
    public function info($table = null, $field = null, $type = "ARRAY_A"){
      $table = ($table) ? $table : $this->tableName;
      $query = "DESCRIBE {$table} {$field}";
      return $this->wpdb->get_results($query, $type);
    }

}
