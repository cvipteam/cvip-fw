<?php if ($datas['search']): ?>
  <form action="" method="GET">
    <?php $this->framework->cvipHtml->adminTable->search_box( __( 'Search' ), $this->framework->appName ); ?>
    <?php if (isset($_GET)): unset($_GET['s']);?>
      <?php foreach ($_GET as $name => $value): ?>
        <input type="hidden" name="<?=$name; ?>" value="<?=esc_attr($value); ?>"/>
      <?php endforeach; ?>
    <?php endif; ?>
  </form>
<?php endif;

@$this->framework->cvipHtml->adminTable->display();
