<div class="cvip-framework">

  <?php if ($this->framework->mainModel && !$this->framework->mainModel->belongsTo): ?>
    <div class="volver">
      <a href="<?=admin_url("admin.php?page=".$_GET['page']); ?>">
        <input type="button" value="&laquo; Volver">
      </a>
    </div>
  <?php endif; ?>


  <div class="header">
    <h1><?=ucfirst($this->framework->cvipHelper->spaceCaps($this->framework->cvipHelper->cleanString($this->framework->controllerName, 'Controller')))     ?></h1>
  </div>

  <div class="table-default">
    <div class="wrap cvip-framework-table">
      <a href="<?="?page={$_REQUEST['page']}&option=add{$datas['tableName']}"; ?>">
        Add
      </a>
      <?=$datas['table']; ?>
    </div>
  </div>

  <br>

  <div class="belongsTo">
    <?php if (isset($datas['belongsTo'])): ?>
      <?php foreach ($datas['belongsTo'] as $tableName => $name): ?>
        <div class="">
          <a href="<?=admin_url("admin.php?page={$this->framework->appName}_".CvipHelper::cleanString($this->framework->controllerName, 'Controller')."&option=belongsTo&tableName=$tableName"); ?>"><?=$name ?></a>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>

</div>
