<div class="cvip-framework">
  <div class="header">
    <h1>DELETE</h1>
  </div>

  <!-- <div class="button-volver">
    <a href="<?php //echo $_SERVER['HTTP_REFERER'] ?>">
      <input type="button" value="&laquo;Volver">
    </a>
  </div> -->

  <h4>INFORMATION</h4>

  <div class="datos contenedor">
  <?php foreach ($datas['info'] as $data): ?>
    <div class="data data-<?=$data?>">
      <?=$data;?>
    </div>
  <?php endforeach; ?>
  </div>

  <a href="#" class="cvip-mvc" data-tableName="<?=$datas['tableName'] ?>" data-plugin="<?=$datas['appName']?>" data-a="admin" data-c="<?=str_replace ( $datas['appName'].'_', '', $_GET['page']) ?>" data-o="<?=$_GET['option']?>" data-id="<?=$datas['primaryKey'] ?>">Delete</a>

</div>
