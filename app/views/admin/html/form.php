<div class="contenedor<?=$tableName??'Form'; ?>">
  <form class="<?=$className??'asyn'; ?>" id="<?=$tableName; ?>" onsubmit="event.preventDefault()">
    <?php foreach ($fields as $key => $campo): ?>
      <div class="campo <?=$key; ?> <?=@$campo['class']; ?>">
        <?php if (isset($campo['label'])): ?>
          <label for="datos[<?=$key; ?>]"> <?=@$campo['label']; ?> </label>
        <?php endif; ?>
        <?=$fieldView[$key]; ?>
      </div>
    <?php endforeach; ?>
    <div class="submit">
      <input type="submit" name="ENVIAR" value="<?=$submit; ?>">
    </div>
  </form>
</div>
