<div class="mediaUpload">
  <input type="text" class="myfile <?="file-$key"; ?>" name="<?=$key; ?>" value="<?=@$field['default']; ?>" placeholder="<?=@$field['placeholder']; ?>" <?=$required; ?> >
  <button class="upload upload_<?=$key?>">SUBIR</button>
  <button class="remove remove_<?=$key?>">&times;</button>

  <script type="text/javascript" style="display:none">
    jQuery(document).ready(function($) {

      // The "Upload" button
      $('.upload_<?=$key; ?>').click(function() {
        var button = $(this);
        wp.media.editor.send.attachment = function(props, attachment) {
          var websiteName = window.location.protocol+"//"+window.location.host;
          var relativeUrl = attachment.url.replace(websiteName, "");
          console.log("FKM:" + websiteName);
            $('.<?=$key?> input').val(relativeUrl);
        }
        wp.media.editor.open(button);
        return false;
      });

      $('.remove_<?=$key; ?>').click(function() {
        var answer = confirm('Seguro?');
        if (answer == true) {
          $('.<?=$key?> input').val("");
        }
        return false;
      });

    });
  </script>
</div>
