<table class="<?=$nombretabla; ?> tabla_<?=$tableName; ?> mitabla" data-paging="true" data-paging-limit="10" data-paging-count-format="Pág. {CP} / {TP}" data-paging-size=<?=$tamano; ?> data-sorting="true" data-filtering="<?=(($buscador)?'true':'false'); ?>" data-filter-dropdown-title="Buscar en:" data-filter-placeholder="Buscar" data-cascade="true" data-use-parent-width="true" data-empty="No hay registros">
  <?php if (isset($datos[0])): ?>
    <?php if ($encabeza): ?>
        <thead>
          <tr>
            <?php foreach ($datos[0] as $campo => $valor): ?>
              <?php if (!in_array($campo,$esconder)): ?>
                <th <?=((in_array($campo,$encoger)) ? 'data-breakpoints="md"' : ""); ?> ><?=$campo; ?></th>
              <?php else: ?>
                <th data-visible="false" data-sortable="false" data-filterable="false"><?=$campo; ?></th>
              <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($modificacion): ?>
              <th data-breakpoints="md" data-name="ACCIONES" data-sortable="false" data-filterable="false">ACCIONES</th>
            <?php endif; ?>
          </tr>
        </thead>
     <?php endif; ?> <!-- # ENCABRZADO -->

     <tbody class="input-filter">
       <?php foreach ($datos as $registro): ?>
         <tr>
           <?php foreach ($registro as $campo => $valor): ?>
             <td class=".sanitize_title(<?=$campo; ?>)."><?=$valor; ?></td>
             <?php if (in_array($campo,$omitir)) unset($registro->$campo); ?>
           <?php endforeach; ?>

         <?php if ($modificacion): ?>
            <td class=acciones>
              <a class="cvip-fw-actions" href="?<?=http_build_query($_GET); ?>&amp;option=edit&amp;<?=$primaryKey; ?>=<?=$registro->{$primaryKey}; ?>">
                <span class="glyphicon glyphicon-pencil" title="EDITAR" style="font-size:25px;">
                  <img alt="edit️" src="<?=plugins_url('cvip-fw/assets/img/pencil-edit-button.svg'); ?>" style="width: 20px;">
                </span>
              </a>
              <>
              <a class="cvip-fw-actions" href="?<?=http_build_query($_GET); ?>&amp;option=delete&amp;<?=$primaryKey; ?>=<?=$registro->{$primaryKey}; ?>">
                <span class="glyphicon glyphicon-remove" title="ELIMINAR" style="color:#dd3333; font-size:25px;">
                  <img alt="Delete" src="<?=plugins_url('cvip-fw/assets/img/trash.svg'); ?>" style="width: 20px;">
                </span>
              </a>

            </td>
          </tr>
         <?php endif; ?>
       <?php endforeach; ?>
     </tbody>
  <?php else: ?>
    <td>No hay registros para mostrar</td>
  <?php endif; ?>
</table>


<script type='text/javascript'>
  <?php if ($funcionAntes): ?>
    jQuery(document).ready(function($) {
      <?="{$funcionAntes};"; ?>
    });
  <?php endif; ?>

  jQuery(function () {
    jQuery('.<?=$nombretabla; ?>').footable();
    <?php if ( isset($_GET['page']) ) { ?>//BOTON AGREGAR
      jQuery(".<?=$nombretabla?> .form-inline").prepend(
        <?php $tableName = (isset($_GET['tableName'])) ? "&tableName={$_GET['tableName']}" : ''; ?>
        "<div class='" + 'button-agregar page-principal ' + "<?=$_GET['page']?>" + "'> <a href='?" + '<?="page={$_GET['page']}&option=add{$tableName}"; ?>' + "'> <img alt='add' src='<?=plugins_url('cvip-fw/assets/img/add-icon.svg'); ?>'> </a> </div>"
      );
    <?php } ?>
  });
</script>
