<select name="<?=$key; ?>" <?=$required; ?> >
  <?=$placeholder; ?>
  <?php foreach ($field['options'] as $value => $nameOption): ?>
    <option value="<?=$value; ?>" <?=($default == $value) ? 'selected' : ''; ?>>
      <?=$nameOption?>
    </option>
  <?php endforeach; ?>
</select>
