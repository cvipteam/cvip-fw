<div class="cvip-framework">
  <div class="header">
    <h1><?=CvipHelper::spaceCaps($this->framework->appName);?></h1>
  </div>

  <div class="adminControllers-list">
    <?php foreach ($datas as $adminController): ?>
      <div class="item item-<?=$adminController?>">
        <a href="<?=admin_url()."admin.php?page={$this->framework->appName}_{$adminController}"?>">
            <?=CvipHelper::spaceCaps($adminController)?>
        </a>
      </div>
    <?php endforeach; ?>

    <div class="item item-<?=$adminController?>">
      <a href="<?=wp_logout_url()?>">
        Exit
      </a>
    </div>
  </div>

</div>
