<h2>BETA</h2>

<div class="BETA">

  <div class="aligncenter" style="text-align: center;">
      <select class="select-user" data-plugin="<?=$this->appName ?>" name="select-user" onchange="selChange" onClick="selChange">
        <option value="0" data-user selected>General user permissions</option>
      <?php foreach ($datas['ControllersEnabledName'] as $key => $adminController):?>
        <option value="<?=$key+1 ?>" data-user="<?=$adminController ?>">
          <?=$this->cvipHelper->spaceCaps($adminController); ?>
        </option>
      <?php endforeach; ?>
      </select>
      <!-- CAMBIAR A INGLES -->
    <div class="container">USA EL DESPLEGABLE DE ARRIBA</div>
  </div>
</div>

<h1><?=$this->cvipHelper->spaceCaps($this->appName); ?></h1>

<h3>Controllers Enabled</h3>

<form method="post" action="options.php">
  <?php settings_fields("cvip-controllers-enabled-{$this->appName}"); ?>
  <table class="form-table">
    <?php foreach ($datas['ControllersName'] as $adminController):?>
    <tr valign="top">
      <th scope="row"><?=$this->cvipHelper->spaceCaps($adminController);?></th>
      <td>
        <input type="checkbox"
          id=<?="cvip-controller-enable-{$this->appName}-{$adminController}" ?>
          name=<?="cvip-controller-enable-{$this->appName}-{$adminController}";?>
          value=<?="cvip-controller-enable-{$this->appName}-{$adminController}";?>
          <?=(get_option("cvip-controller-enable-{$this->appName}-{$adminController}"))?'checked':'';?>
        />
      </td>
    </tr>
    <?php endforeach; ?>
  </table>
  <?php submit_button('Save'); ?>
</form>
