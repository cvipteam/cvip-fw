<h2>BETA</h2>

<div class="BETA">
  <div class="aligncenter" style="text-align: center;">
      <select class="select-user" data-plugin="<?=$this->framework->appName ?>" name="select-user" onchange="selChange" onClick="selChange">
        <option value="0" data-user selected disabled>User Permissions</option>
      <?php foreach ($datas['controllersName'] as $key => $adminController):?>
        <option value="<?=$key+1 ?>" data-user="<?=$adminController ?>">
          <?=CvipHelper::spaceCaps($adminController)?>
        </option>
      <?php endforeach; ?>
      </select>
      <!-- CAMBIAR A INGLES -->
    <div class="container">USA EL DESPLEGABLE DE ARRIBA</div>
  </div>
</div>

<h1><?=CvipHelper::spaceCaps($this->framework->appName); ?></h1>
<h3>Controllers Enabled</h3>

<form method="post" action="options.php">
  <?php settings_fields("cvip-controllers-enabled-{$this->framework->appName}"); ?>
  <table class="form-table">
    <?php foreach ($datas['controllersEnabledName'] as $adminController):?>
    <tr valign="top">
      <th scope="row"><?=CvipHelper::spaceCaps($adminController);?></th>
      <td>
        <input type="checkbox"
          id=<?="cvip-controller-enable-{$this->framework->appName}-{$adminController}" ?>
          name=<?="cvip-controller-enable-{$this->framework->appName}-{$adminController}";?>
          value=<?="cvip-controller-enable-{$this->framework->appName}-{$adminController}";?>
          <?=(get_option("cvip-controller-enable-{$this->framework->appName}-{$adminController}"))?'checked':'';?>
        />
      </td>
    </tr>
    <?php endforeach; ?>
  </table>
  <?php submit_button('Save'); ?>
</form>

<hr>

<h1>User <?=$datas['userTitle']; ?></h1>
 <form method="post" action="options.php">
   <!-- TODO: CITIES VER ESTRUCTURA -->
   <?php settings_fields("cvip-user-{$this->framework->appName}-{$_GET['user']}"); ?>
        <?php foreach (CvipHelper::getControllersName($this->framework->controllersEnabled) as $user): ?>
          <button class="accordion">
            <div class="title"><?=ucfirst($user);?> Controller</div>
            <div class="status">(<?=(get_option("cvip-user-enabled-{$_GET['user']}-$user")) ? 'Enabled': 'Disabled'; ?>)</div>
          </button>

          <div class="accordion-panel">
            <div class="cvip-controller-enabled">
              <h3>Enabled / Disabled</h3>
              <input
                type="checkbox"
                id=<?="cvip-user-enabled-{$_GET['user']}-$user" ?>
                name=<?="cvip-user-enabled-{$_GET['user']}-$user";?>
                value=<?="cvip-user-enabled-{$_GET['user']}-$user";?>
                <?=(get_option("cvip-user-enabled-{$_GET['user']}-$user"))?'checked':'';?>
              />
            </div>

            <div class="cvip-metodos">
              <?php foreach ($this->framework->metodos[$user] as $metodo): ?>
                <div class="cvip-container-option">
                  <div class="cvip-container-option-name">
                    <?=ucfirst($metodo);?>:
                  </div>
                  <div class="cvip-container-option-input">
                    <input type="checkbox"
                      id=<?="cvip-user-{$_GET['user']}-{$user}-{$metodo}" ?>
                      name=<?="cvip-user-{$_GET['user']}-{$user}-{$metodo}";?>
                      value=<?="cvip-user-{$_GET['user']}-{$user}-{$metodo}";?>
                      <?=(get_option("cvip-user-{$_GET['user']}-{$user}-{$metodo}"))?'checked':'';?>
                    />
                  </div>
                </div>
                <br>
            <?php endforeach; ?>
          </div>
        </div>

      <?php endforeach; ?>
   <?php submit_button('Save'); ?>
 </form>


<script>

</script>
