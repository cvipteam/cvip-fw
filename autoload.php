<?php
# FUNCION PARA AUTO CARGAR CLASES
spl_autoload_register(function ($class) {
  $class = str_replace("\\", DIRECTORY_SEPARATOR, $class);

  $fw_path  = CVIP_FW_PATH . DIRECTORY_SEPARATOR . $class . '.php';

  // $app_path = $this->paths->appPath . DIRECTORY_SEPARATOR . $class . '.php';

  $app_path_pubController = $this->paths->appControllerPath . $class . '.php';
  $app_path_adminController = "{$this->paths->appControllerPath}admin/" . $class . '.php';

  $app_path_models = $this->paths->modelsPath . $class . '.php';

  if (file_exists($fw_path)){
    require_once($fw_path);
  }
  // else if (file_exists($app_path)){ // QUESTION: PONER ADMIN CONTROLLER?
  //   require_once($appPath);
  // }
  else if (file_exists($app_path_pubController)){ # PUB CONTROLLER
    require_once($app_path_pubController);
  } else if (file_exists($app_path_models)){ # APP MODELOS
    require_once($app_path_models);
  }
});
