<?php
/*
 * Plugin Name: CVIP Framework
 * Plugin URI: ColombiaVIP.com
 * Description: Framework para sistemas.
 * Version: 20210301
 * Author: Colombia VIP
 * Author 2: Flikimax
 * Author URI: ColombiaVIP.com
 * Documentation: ColombiaVIP.com
 * License: PRIVADA
 */

# NOTE: SE REESTRUCTURARÁ LA PARTE DE PERMISOS, USUARIOS, CADA UNO CON SU RESPECTIVA CLASE

define('CVIP_DEBUG', true); # PARA DESARROLLO
defined ('ABSPATH') or die ('Sorry, you are not allowed to access this page.');

use
  app\controllers\admin\adminController,
  app\controllers\pubController,
  settingsPage\settingsPage,
  helpers\cvipHelper,
  helpers\loadAssets,
  helpers\views\cvipHtml,
  helpers\views\adminTable;

class cvip_framework{
  function __construct($pluginPath) {
    if (!defined('CVIP_FW_PATH')) define('CVIP_FW_PATH', dirname(__FILE__)); # RUTA DEL FRAMEWORK - GLOBAL
    if (!defined('CVIP_FW_VERSION')) define('CVIP_FW_VERSION', (defined('CVIP_DEBUG') && CVIP_DEBUG) ? time() : cvipHelper::getVersion(__FILE__));
    $this->appName = basename($pluginPath, '.php'); # NOMBRE APP

    require_once "helpers/paths.php";
    $this->paths = createPaths($pluginPath, $this->appName); # CREACION DE RUTAS FW Y APP
    require_once 'autoload.php';

    add_action('init', array($this, 'init'));
    add_action('admin_init', array($this, 'admin_init'));
    add_action('wp_login', array($this, 'loginRedirect')); # REDIRECCION Al HACER LOGIN

    register_activation_hook($this->paths->pluginPath, array($this, 'cvip_activation')); # ACTIVACION PLUGIN
    register_deactivation_hook($this->paths->pluginPath, array($this, 'cvip_deactivation')); # DESACTIVACION PLUGIN
  }

  public function init() {
    // TODO: PASAR LOS NEW HELPES X,Y,X A $this->helpers->main
    // TODO: PASAR LOS NEW HELPES X,Y,X A $this->helpers->html
    $this->cvipHtml   = new cvipHtml;
    $this->cvipHelper = new cvipHelper;
    $this->loadAssets = new loadAssets($this->paths);
    // require_once 'cvipUsers.php'; # TODO: PASAR A CLASE
    // cvip_gl_add_role($this->appName, $this->adminControllersFiles); # TODO: PASAR EL ARCHIVO cvipUsers A CLASE

    // TODO: REGISTRAR SETTINGS (OPTIONS) - PERMISOS
    $this->controllersEnabled();

    // $this->loadAssets->registerStyleScript_controller($this->paths->appName);
    add_action('wp_ajax_nopriv_cvip_ajax_mvc', array($this, 'cvip_pub_ajax_mvc'), 10 ); # AJAX PUBLICO
    add_shortcode( "cvip_$this->appName" , array($this, 'shortcode_cvip_mvc')); # SHORTCODES

    add_action('admin_menu', array($this, 'registerAdminMenuPages')); #  ADMIN MENU PRINCIPAL, SUB PAGE Y SETTING PAGE
  }

  public function admin_init(){
    add_action('wp_ajax_cvip_ajax_mvc', array($this, 'cvip_ajax_mvc'), 10 ); # AJAX ADMIN
  }


  # = METODO QUE SOLICITA PAGINA PRINCIPAL Y SUB PAGINAS =
  function registerAdminMenuPages() {
    if (is_admin()) $this->cvipHtml->adminTable = new adminTable;

    new SettingsPage([ # PAGINA DE AJUSTES DE CADA APLICACIÓN
      'appName' => $this->appName,
      'adminControllersFiles' => $this->paths->adminControllersFiles,
      'controllersEnabled' => $this->controllersEnabled
    ]);

    # PAGINA PRINCIPAL DE LA APLICACION
    $adminAppNiceName = $this->cvipHelper->spaceCaps($this->appName);
    add_menu_page($adminAppNiceName, $adminAppNiceName, 'administrator', $this->appName, function () {
      echo $this->loadController([
        'a' => $_GET['a']??'admin',
        'c' => file_exists("{$this->paths->appControllerPath}admin/{$this->appName}Controller.php") ? $this->appName : (isset($_GET['c']) ? @$_GET['c'] : 'Admin'),
        'o' => $_REQUEST['option'] ?? $_GET['o'] ?? 'mainPage'
      ]);
    }, null, 6);

    # SE QUITA EL CONTROLADOR PRINCIPAL DE LA APP
    if ( in_array("{$this->paths->appControllerPath}admin/{$this->appName}Controller.php", $this->paths->adminControllersFiles) )
      unset($this->paths->adminControllersFiles[array_search("{$this->paths->appControllerPath}admin/{$this->appName}Controller.php", $this->paths->adminControllersFiles)]);
    if ( array_search($this->appName, $this->controllersEnabled) )
      unset($this->controllersEnabled[array_search($this->appName, $this->controllersEnabled)]);

    # SUB PAGINAS DE LA APLICACION
    foreach ($this->controllersEnabled as $adminController){
      $adminControllerName = $this->cvipHelper->cleanString($adminController, "Controller");
      $adminControllerNiceName =$this->cvipHelper->spaceCaps($adminControllerName);
      add_submenu_page( $this->appName, $adminControllerNiceName, $adminControllerNiceName, 'administrator',  $this->appName."_".$adminControllerName,
          function() use ($adminControllerName) {
            echo $this->loadController([
              'a' => 'admin',
              'c' => $adminControllerName,
              'o' => @$_REQUEST['option'],
            ]);
          }
      );
    }
  }


  /**
   * loadController Method
   *
   *
   * @param Array Data=>[a=>Access,c=>Controller,o=>Option,...(args)]
   * @return return type
   */
  public function loadController($args){
    ob_start();
    if (isset($args['a']) && $args['a'] == 'admin') $this->paths->appControllerPath.="admin/";
    $this->controllerName = (isset($args['c'])) ? $args['c'].'Controller' : NULL;
    $option = $args['o'] ?? 'default';

    if ( file_exists("{$this->paths->appControllerPath}{$this->controllerName}.php") && class_exists($this->controllerName) ){
      $controller = new $this->controllerName($this);
    } else if ( (in_array("{$this->paths->appControllerPath}{$this->controllerName}.php", $this->paths->adminControllersFiles)) ){ # adminController
      $controller = new adminController($this);
    } else { # pubController
      $controller = new pubController($this);
    }

    unset(
      $args['o'],
      $args['plugin'],
      $args['c'],
      $args['action'],
      $args['a'],
      $args['allDatas'],
    );

    // $this->parameter = $args;
    $controller->parameter = $args;

    if (defined('CVIP_DEBUG')) cvipHtml::sendToConsole("Controller Name: {$controller->framework->controllerName}");

    $this->loadAssets->registerStyleScript_controller($this->paths->appName);
    $this->loadAssets->registerStyleScript_controller(cvipHelper::cleanString($controller->framework->controllerName, 'Controller'));
    (method_exists( $controller, $option )) ? $controller->{$option}() : $controller->default();
    return ob_get_clean();
  }


  # AJAX ADMIN
  function cvip_ajax_mvc() {
    $this->appName    = $_POST['plugin'];
    $this->pluginPath = "{$this->paths->appPath}.php";
    $this->appPath    = $this->paths->appName;
    $this->appControllerPath = $this->paths->appControllerPath;

    if (isset($_POST['allDatas'])){
      $args = (array) json_decode( str_replace('\\', '', $_POST['allDatas']) );
    }

    $_POST['a'] = (isset($_POST['a']) && $_POST['a'] == 'admin') ? $_POST['a'] : 'pub';
    $args = (isset($args) && $args) ? array_merge_recursive($_POST, $args) : $_POST;
    die($this->loadController($args));
  }

  // TODO: AGREGAR MEJORAS DEL AJAX ADMIN
  # AJAX PUBLIC OR USER NOT LOGGEDIN
  function cvip_pub_ajax_mvc() {
    unset($this->adminControllersFiles);
    $this->appName = $_POST['plugin'];
    $this->pluginPath = "{$this->paths->appPath}.php";
    $this->appPath    = $this->paths->appName;
    // $this->appPath = dirname( __DIR__ ).'/'.$this->appName;
    // $this->appControllerPath = $this->paths->appPath.'/app/controllers/'; // TODO: OPTIMIZAR
    $this->appControllerPath = $this->paths->appControllerPath;

    die($this->loadController($_POST));
  }

  function shortcode_cvip_mvc($atts) {
    $atts = array_merge( array(
      'a' => 'pub',
      'c' => null,
      'o' => null,
    ), $atts);
    unset($this->paths->adminControllersFiles);
    return $this->loadController($atts);
  }


  # = REDIRECCION Al HACER LOGIN =
  public function loginRedirect() {
    $user = get_user_by('login', $_POST['log']);
  	$url = admin_url('/edit.php');

    if ($user->roles[0] == 'administrator' || $user->roles[0] == strtoupper($this->appName)) {
      $url = admin_url("admin.php?page={$this->appName}");
    }else if (get_option("cvip-controller-enable-{$this->appName}-{$user->roles[0]}") && get_option("cvip-user-{$user->roles[0]}-{$user->roles[0]}-'default'")){
      foreach ($this->cvipHelper->getControllersName($this->adminControllersFiles) as $adminController) {
        if ($user->roles[0] == $adminController)
          $url = admin_url("/admin.php?page={$this->appName}_{$adminControllerName}");
      }
    }
  	if (wp_redirect($url))
  		exit;
  }


  // TODO: VALIDAR CON LAS SETTINGS GUARDADAS
  public function controllersEnabled(){
    foreach ($this->cvipHelper->getControllersName($this->paths->adminControllersFiles) as $adminController) {
      $this->controllersEnabled[] = $adminController;
      require_once "{$this->paths->appControllerPath}admin/{$adminController}Controller.php";
    }
  }


  # METODO QUE SE EJECUTA AL ACTIVAR EL PLUGIN
  function cvip_activation (){
    cvip_gl_add_role($this->appName, $this->adminControllersFiles);
    flush_rewrite_rules();
  }



  # METODO QUE SE EJECUTA AL DESACTIVAR EL PLUGIN
  function cvip_deactivation (){
    cvip_gl_remove_role($this->appName, $this->adminControllersFiles);
    flush_rewrite_rules();
  }

}

# SE AGREGA EL AUTOR FLIKIMAX
add_filter('extra_plugin_headers', 'add_extra_headers');
function add_extra_headers(){
  return array('Author 2', 'Documentation');
}
add_filter('plugin_row_meta', 'filter_authors_row_meta', 1, 4);
function filter_authors_row_meta($plugin_meta, $plugin_file, $plugin_data, $status){
  if ( !empty( $plugin_data['Author 2'] ) )
    $plugin_meta[1] = $plugin_meta[1] . '<span> & <span>' . "<a href='http://flikimax.com'>{$plugin_data['Author 2']}</a>";
  if ( !empty( $plugin_data['Documentation'] ) )
    $plugin_meta[2] = "<a href='{$plugin_data['Documentation']}'>".__('Documentation')."</a>";
  return $plugin_meta;
}
