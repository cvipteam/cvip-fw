<?php
namespace helpers;

class cvipHelper {
  
  public static function getControllersName($controllers){
    if (!$controllers) return;
    $controllersName = null;
    foreach ($controllers as $controller) {
      $controllersName[] = self::cleanString($controller, "Controller");
    }
    return $controllersName;
  }

  public static function fecha($format = 'Y-m-d H:i:s', $timeZone = 'America/Bogota'){
    date_default_timezone_set($timeZone);
    return date($format);
  }

  public static function getVersion($plugin_file){
    $plugin_data = get_file_data($plugin_file, array('Version' => 'Version'), false);
    return $plugin_data['Version'];
  }

  public static function cleanString($string, $remove){
    return str_replace ( $remove, '',  basename($string, '.php'));
  }

  public static function spaceCaps($string){
    return ucfirst ( preg_replace( '/[A-Z]/', ' $0',  $string) );
  }

  public static function permisos($listado=FALSE){
    foreach ($listado as $key => $value) {
       if (current_user_can($value)) {
         return;
       }
    }
    die("No tienes permiso para estar en esta area.");
  }

  // TODO: REVISAR QUE SIRVE Y ELIMINAR - SE CREARA UNA CLASE PERMISOS
  public static function permissions($args = FALSE){
    if (!$args)
      return FALSE;

    foreach ($args as $key => $user) {
       if (!current_user_can($user))
         die("No tienes permiso para estar en esta area.");
    }
    return TRUE;
  }

  public static function getCurrentUserRole(){
    if (is_user_logged_in()){
      $user = wp_get_current_user();
      return $user->roles[0];
    }
    return null;
  }


}
