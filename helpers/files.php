<?php
namespace helpers;

class files {
  function __construct($args = null){
    $this->model  = $args['model'];
    $this->helper = $args['helper'];
    if ($_FILES){
      $this->valFiles();
    }
  }

  public function valFiles(){
    try {
      foreach ($_FILES as $key => $metas) {
        if (isset($this->model->fields[$key]['path']) && !empty($this->model->fields[$key]['path'])){
          if (!isset($metas['error']) || is_array($metas['error'])) {
            throw new RuntimeException('Invalid parameters.');
          }

          # Check $_FILES['upfile']['error'] value.
          switch ($metas['error']) {
              case UPLOAD_ERR_OK: # Valor: 0; No hay error, fichero subido con éxito.
                break;
              case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file was uploaded');
              case UPLOAD_ERR_INI_SIZE:
                throw new RuntimeException('The uploaded file exceeds the upload_max_filesize directive of php.ini.');
              case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');

              case UPLOAD_ERR_PARTIAL:
                throw new RuntimeException('The uploaded file was only partially uploaded');
              case UPLOAD_ERR_NO_TMP_DIR:
                throw new RuntimeException('Missing a temporary folder');
              case UPLOAD_ERR_CANT_WRITE:
                throw new RuntimeException('Failed to write file to disk');
              case UPLOAD_ERR_EXTENSION:
                throw new RuntimeException('File upload stopped by extension');

              default:
                throw new RuntimeException('Unknown errors.');
          }

          # SE COMPRUEBA EL TAMAÑO DE LOS ARCHIVOS
          $size = (isset($this->model->fields[$key]['size']) && $this->model->fields[$key]['size']) ? $this->model->fields[$key]['size'] : 4000000;
          if ($metas['size'] > $size) {
            throw new RuntimeException('Exceeded filesize limit.');
          }

          # NO CONFIAR EN EL VALOR DE $_FILES['upfile']['mime'] ($metas)
          # COMPROBAR MANUALMENTE EL TIPO MIME
          $type = (isset($this->model->fields[$key]['type']) && $this->model->fields[$key]['type']) ? $this->model->fields[$key]['type'] : array('jpg' => 'image/jpeg', 'png' => 'image/png');
          $finfo = new finfo(FILEINFO_MIME_TYPE);
          if (false === $ext = array_search($finfo->file($metas['tmp_name']), $type, true)) {
            throw new RuntimeException('Invalid file format.');
          }

          # PONER NOMBRE UNICO
          # VALIDAR $_FILES['upfile']['name'] ($metas)
          $siteName = $this->helper->cleanString(site_url(), 'http://');
          $siteName = $this->helper->cleanString(site_url(), 'https://');

          $basePath  = ABSPATH . 'wp-content/uploads/' . $this->model->fields[$key]['path'];
          $cleanName = sanitize_text_field(basename($metas['name']));

          $pathFile = $basePath . $cleanName;
          # SE INTENTA MOVER EL ARCHIVO
          if (!move_uploaded_file($metas['tmp_name'], $pathFile)) {
            throw new RuntimeException('Failed to move uploaded file.');
          }

          $newName = dirname($basePath) . '/' . $this->model->fields[$key]['path'] . rand ('1000', '100000') . '-' . $cleanName;
          rename($pathFile, $newName);

          $this->pathFileTemp = $newName;
        }
      }
    } catch (RuntimeException $e) {
      echo $e->getMessage(); // wp_die('Se ha producido un error: '.$e->getMessage());
    }
  }


}
