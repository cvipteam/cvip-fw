<?php
namespace helpers;
use app\models\mainModel as mainModel;
use helpers\views\cvipHtml as cvipHtml;

class loadAssets {
  function __construct($paths){
    $this->paths = $paths;
    # JS Y CSS - FW
    add_action('wp_enqueue_scripts', array($this, 'fw_style_scripts')); # PUBLICOS
    add_action('admin_enqueue_scripts', array($this, 'fw_style_scripts')); # ADMIN
  }

  public function fw_style_scripts(){
    // IDEA: IF DEFINE ALL ASSETS OR DEFINE ARRAY ASSETS TO LOADER
    $this->all_css_js(false);
  }


  // TODO: PASAR A HELPERS\LOADASSETS
  # = RETORNA UN ARRAY DE OBJETOS DE MODELOS =
  public function loadModels($models){
    if (is_array($models)){
      $arrayModels = [];
      foreach ($models as $model) {
        $arrayModels[$model] = $this->loadModel($model);
      }
      return $arrayModels;
    }
  }


  # = CARGA Y RETORNA UN MODELO =
  public function loadModel($modelName){
    $modelName = str_replace ( 'Controller', '', $modelName );
    $modelPath = "{$this->paths->modelsPath}{$modelName}.php";

    $model = (file_exists($modelPath) && class_exists($modelName)) ? new $modelName($modelName) : new mainModel($modelName);
    if (empty($model->tableName)){
      if (defined('CVIP_DEBUG')){
        cvipHtml::sendToConsole('Warning: The Main Model no exist.', 'warn');
      }
      return null;
    }
    return $model;
  }


  public function all_css_js($app = true, $css = true, $js = true){
    $path = ($app) ? $this->paths->appAssetsPath : $this->paths->fwAssetsPath;

    if ($css){
      $filesCss = glob($path.'css/*.css');
      foreach ($filesCss as $key => $fileCss) {
        $this->registerStyle( basename($fileCss, false) );
      }
    }
    if ($js){
      $filesJs = glob($path.'js/*.js');
      foreach ($filesJs as $key => $fileJs) {
        $fileJs = basename($fileJs);
        $ajax = (strpos($fileJs, 'ajax') !== false) ? true : false;
        $this->registerScript($fileJs, $ajax, false);
      }
    }
  }


  # REGISTRA UN STYLE
  function registerStyle ($style, $app = true){
    if (!$this->asset_exists($style, $app)) return;
    $path = ($app) ? $this->paths->appWebPath : $this->paths->fwWebPath;
    wp_register_style('fw_'.$style, "{$path}css/{$style}", array(), CVIP_FW_VERSION);
    wp_enqueue_style('fw_' .$style);
  }


  # REGISTRA UN SCRIPT
  function registerScript ($js, $ajax = false, $app = true){
    if (!$this->asset_exists($js, $app)) return;
    $path = ($app) ? $this->paths->appWebPath : $this->paths->fwWebPath;
    wp_register_script('fw'.$js, "{$path}js/{$js}", array(), CVIP_FW_VERSION, TRUE);
    wp_enqueue_script('fw' .$js);

    if ($ajax){
      if (is_string($ajax)){
        $ajax_var['var'] = $ajax;
      } else if (is_array($ajax)){
        $ajax_var = $ajax;
      }
      $ajax_var['ajaxurl'] = admin_url('admin-ajax.php');
      wp_localize_script('fw'.$js, 'WP_AJAX', $ajax_var);
    }
  }


  # REGISTRA EL STYLE Y UN SCRIPT PARA UN CONTROLADOR, USANDO SU NOMBRE
  public function registerStyleScript_controller($controllerName){
    $this->registerStyle("$controllerName.css");

    $ajax = (strpos($controllerName, 'ajax') !== false) ? true : false; # SI controllerName CONTIENE LA PALABRA ajax
    $this->registerScript("$controllerName.js", $ajax);
  }


  # VERIFICA SI UN ASSET EXISTE
  public function asset_exists($assetName, $app = true, $subFolder = null){
    $path = ($app) ? $this->paths->appAssetsPath : $this->paths->fwAssetsPath;
    $path .= ($subFolder) ? trim($subFolder, '/') . "/$assetName" : pathinfo($assetName, PATHINFO_EXTENSION) . "/$assetName";

    return (file_exists($path)) ? true : false;
  }

}
