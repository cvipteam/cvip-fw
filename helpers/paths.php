<?php
function createPaths($pluginPath, $appName){
  $appPath = dirname($pluginPath);
  $appControllerPath = $appPath . '/app/controllers/'; # RUTA CONTROLADORES
  return (object) array(
    'pluginPath' => $pluginPath, # ARCHIVO RAIZ APP
    'appPath' => $appPath, # RUTA APP
    'appName' => $appName, # NOMBRE APP

    'appControllerPath' => $appControllerPath, # RUTA CONTROLADORES
    'adminControllersFiles' => glob($appControllerPath.'admin/*.php'), # RUTA CONTROLADORES ADMIN
    'modelsPath' => $appPath . '/app/models/',

    'pathAssetrelative' => '/app/assets/',
    'appServerPath' => $appPath,
    'fwServerPath'  => CVIP_FW_PATH,
    'appAssetsPath' => $appPath . '/app/assets/',
    'fwAssetsPath'  => CVIP_FW_PATH . '/app/assets/',
    'appWebPath' => plugins_url(basename($appPath) . '/app/assets/'),
    'fwWebPath' => plugins_url(basename(CVIP_FW_PATH) . '/app/assets/'),
  );
}
