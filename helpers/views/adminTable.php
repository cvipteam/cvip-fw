<?php
namespace helpers\views;

if(!class_exists('WP_List_Table')) require_once(ABSPATH.'wp-admin/includes/class-wp-list-table.php');

class adminTable extends \WP_List_Table {
    private $model = null;
    private $options = null;

    /**
     * Prepare the items for the table to process
     * @return Void
    */
    public function prepare_items($args = null) {
      if ($args['model'] && $args['datas']){
        $this->model = $args['model'];
        $searchWhere  = (isset($args['searchWhere'])) ? $args['searchWhere'] : null;
        $this->items = ($this->model->search && isset($_GET['s']) && $_GET['s'] && !empty(trim($_GET['s']))) ? $this->model->search($this->model->search, trim($_GET['s']), $searchWhere) : $args['datas'];
        $hidden = (isset($args['hidden']) && is_array($args['hidden']))  ? $args['hidden'] : $this->model->getPrepareHiddenColumns();
        $columns = isset($args['columns']) ? $args['columns'] : $this->model->getPrepareColumns();
        $sortable = (isset($args['sortableColumns']) && is_array($args['sortableColumns'])) ? $this->get_sortable_columns($args['sortableColumns']) : $this->get_sortable_columns(array_keys($columns));
        $perPage = isset($args['pagination']) ? $args['pagination'] : 20;

        # SE AGREGAN LAS OPCIONES
        if (isset($args['options']) && is_array($args['options'])){
          $this->options = $args['options'];
          $columns['options'] = __('Actions');
        }

        $this->columnActios = @array_shift(array_values($columns));
        if ($this->items)
          usort( $this->items, array( &$this, 'sort_data' ) );
        $this->set_pagination_args(array(
            'total_items' => @count($this->items),
            'per_page'    => $perPage
        ));
        $currentPage = $this->get_pagenum();
        if ($this->items)
          $this->items = array_slice($this->items,(($currentPage-1)*$perPage),$perPage);

        return $this->_column_headers = array($columns, $hidden, $sortable);
      }
      return;
    }

    /**
     * Define the sortable columns
     * @return Array
     * NOTE: PARA ORDENAR LAS TABLAS
    */
    public function get_sortable_columns($columns = null){
      if (!$columns)
        return;
      foreach ($columns as $column) {
        $sortable_columns[$column] = array($column);
      }
      return $sortable_columns;
    }

    /**
     * Define what data to show on each column of the table
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name ){
      if (isset($item[$column_name]) && $item[$column_name]){
        return $item[$column_name];
      } else {
        if ($column_name == 'options'){
          $tableName = (isset($_GET['tableName']) && $_GET['tableName']) ? "&tableName={$_GET['tableName']}" : '';

          $options = array();
          $parameters = '';
          foreach ($this->options as $function => $args) {
            $label = (isset($args['label']) && $args['label']) ? $args['label'] : $function;
            if (isset($args['args']) && $args['args'])
              foreach ($args['args'] as $var => $value)
                $parameters .= "&$var=$value";
            $options[$function] = sprintf('<a href="?page=%s&option=%s%s&%s=%s%s">%s</a>',$_REQUEST['page'], $function, $tableName, $this->model->primaryKey, $item[$this->model->primaryKey], $parameters, $label);
          }
          return sprintf('%1$s ',  $this->row_actions($options) );
        }
        return "-";
      }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     * @return Mixed
     */
    private function sort_data($a, $b){
      // Set defaults
      $orderby = $this->model->primaryKey;

      $order = 'asc';
      // If orderby is set, use this as the sort column
      if(!empty($_GET['orderby'])){
        $orderby = $_GET['orderby'];
      }
      // If order is set use this as the order
      if(!empty($_GET['order'])){
        $order = $_GET['order'];
      }
      $result = strnatcmp( $a[$orderby], $b[$orderby] );

      if($order === 'asc'){
        return $result;
      }
      return -$result;
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     * @return Array
     */
    public function get_columns(){
        return array();
    }

    /**
     * Define which columns are hidden
     * @return Array
     */
    public function get_hidden_columns(){
        return array();
    }
}
