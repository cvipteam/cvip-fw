<?php
 # ======================================================================
 # =                             CLASE HTML                             =
 # = CONTIENE LOS METODOS PARA GENERAR ELEMENTOS HTML DE FORMA DINAMICA =
 # ======================================================================

namespace helpers\views;

class cvipHtml {

  public $html_Templates_Path = CVIP_FW_PATH.'/app/views/admin/html/';

  # CREAR UNA TABLA
  public function createTable($args){
    ob_start();
    $datos        = FALSE; # = DATOS
    $limit        = '10000'; # LIMITE DE RESULTADOS POR CONSULTA (NO SE NECESITA SI SE PASA LA CONSULTA) // TODO: TESTEAR
    $tamano       = '25'; # = CANTIDAD DE RESULTADOS POR PAGINA
    $omitir       = array(); # = ARRAY A OMITIR EN EDICIÓN ÚTIL CUANDO SE RENOMBRE LA COLUMNA EN EL QUERY
    $encoger      = array(); # = ARRAY DE COLUMNAS A ENCOGE EN MOVIL
    $encabeza     = true; # = TITULO DE COLUMNAS
    $buscador     = false; # = BUSCADOR
    $esconder     = array(); # = ARRAY DE COLUMNAS A ESCONDER
    $tableName;   # = NOMBRE DE LA TABLA =
    $htmlTable    = ''; # = HTML QUE SERA ENVIADO A LA VISTA =
    $primaryKey   = null;
    $nombretabla  = 'tabla'.rand(1,100); # = ES RANDOM PARA EVITAR CONFLICTOS SI SE CREAN MAS DE 2 TABLAS EN UNA WEB
    $funcionAntes = false; # = SCRIP ANTES DE MOSTRAR LA TABLA
    $modificacion = false; # = PERMITE EDICION
    extract($args);

    require $this->html_Templates_Path.'pubTable.php';
    return ob_get_clean();
  }

  # GENERA UN FORM AUTOMATICO CON TODOS SUS CAMPOS
  public function form($fields){
    ob_start();
    $submit = (isset($fields['submit'])) ? $fields['submit'] : 'SEND';
    $tableName = $fields['model']['default'];
    $className = $fields['className'];
    $fieldView = [];
    unset($fields['submit'], $fields['className']);

    foreach ($fields as $key => $field) {
      switch (@$field['type']) {
        case 'select':
          $fieldView[$key] = CvipHtml::select($key, $field);
        break;
        case 'textarea':
          $fieldView[$key] = CvipHtml::textarea($key, $field);
        break;
        case 'media':
          $fieldView[$key] = CvipHtml::media($key, $field);
        break;
        default:
          $fieldView[$key] = CvipHtml::input($key, $field);
        break;
      }
    }

    require $this->html_Templates_Path.__FUNCTION__.'.php';
    return ob_get_clean();
  }

  public function select($key, $field){
    ob_start();
    $required = (isset($field['required']) && ($field['required'])) ? 'required': '';

    // TODO: VALIDAR SI EXISTE EL DEFAULT, USARLO, SI NO, VALIDAR EL PLACEHOLDER COMO ESTA ABAJO



    // if (isset($field['default']))
    //   $default = $field['default'];

    $default = (isset($field['default'])) ? $field['default'] : false;
    $selected = (!$default) ? 'selected' : '';
    $placeholder = (isset($field['placeholder']) && !empty($field['placeholder'])) ? '<option value="" '.$selected.' disabled>'. $field['placeholder']??'Click &#x25BC;' .'</option>' : '';

    require $this->html_Templates_Path.__FUNCTION__.'.php';
    return ob_get_clean();
  }

  public function textarea($key, $field){
    ob_start();
    $required = (isset($field['required']) && ($field['required'])) ? 'required' : '';
    $disabled = (isset($field['disabled']) && ($field['disabled'])) ? "disabled" : '';

    require $this->html_Templates_Path.__FUNCTION__.'.php';
    return ob_get_clean();
  }

  function input($key, $field){
    ob_start();
    $type = $field['type']??'text';
    $required = (isset($field['required']) && ($field['required'])) ? 'required' : '';
    $disabled = (isset($field['disabled']) && ($field['disabled'])) ? 'disabled' : '';

    require $this->html_Templates_Path.__FUNCTION__.'.php';
    return ob_get_clean();
  }

  function media($key, $field){
    wp_enqueue_media();
    ob_start();
    $required = (isset($field['required']) && ($field['required'])) ? 'required' : '';

    require $this->html_Templates_Path.__FUNCTION__.'.php';
    return ob_get_clean();
  }

  public static function sendToConsole($debugOutput, $type = 'log'){
    $cleanedString = '';
    if ($type != 'log' && $type != 'warn' && $type != 'error') $type = 'log';
    if (!is_string($debugOutput)) $debugOutput = print_r($debugOutput, true);
    $str_len = strlen($debugOutput);
    for($i = 0; $i < $str_len; $i++){
      $cleanedString .= '\\x' . sprintf('%02x', ord(substr($debugOutput, $i, 1)));
    }
    $javascript_ouput = "<script>console.".$type."('$cleanedString');</script>";
    echo $javascript_ouput;
  }

}
