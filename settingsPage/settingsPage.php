<?php
namespace settingsPage;
use helpers\cvipHelper;

class settingsPage {
  private $appName;
  private $adminControllersFiles;
  private $controllersEnabled;

  function __construct($args){
    if ($args['adminControllersFiles'] && $args['appName']){
      $this->adminControllersFiles = $args['adminControllersFiles'];
      $this->controllersEnabled = $args['controllersEnabled'];
      $this->cvipHelper = new cvipHelper;
      $this->appName = $args['appName'];
      $this->addOptionsPage();
    } else {
      echo "<h1>Sin controladores agregados.<h1>";
    }
  }

  public function addOptionsPage(){
    add_options_page(
      "CVIP " . cvipHelper::spaceCaps($this->appName),
      "CVIP " . cvipHelper::spaceCaps($this->appName),
      strtoupper($this->appName), "cvip-$this->appName",
      array($this, 'controller')
    );
  }

  public function controller(){
    $option = isset($_GET['option']) ? $_GET['option'] : 'default';
    (method_exists( $this, $option )) ? $this->{$option}() : $this->default();
  }







  # ==================
  # ====== BETA ======
  # ==================
  # = PAGINA CONFIGURACION DE LA APLICACION =
  public function default(){
    echo $this->loadView('settingsPage', [
      'ControllersName' => CvipHelper::getControllersName($this->adminControllersFiles),
      'ControllersEnabledName' => CvipHelper::getControllersName($this->controllersEnabled)
    ], 'admin');
  }

  public function loadView($view, $datas){
    ob_start();
    require (CVIP_FW_PATH . "/app/views/admin/settingsPage/{$view}.php");
    return ob_get_clean();
  }

  public function users(){
    if (isset($_GET['user'])){
      $userTitle = ucfirst(strtolower($_GET['user']));
      $user = $_GET['user'];
      foreach ($this->framework->controllersEnabled as $controller) {
        if ($controller && !(CvipHelper::cleanString($controller, "Controller") == $user)){
          $adminControllerName[] = CvipHelper::cleanString($controller, "Controller");
        }
      }

      echo "<pre>adminControllersFiles: ";
      print_r( $this->adminControllersFiles );
      echo "</pre>";

      echo $this->loadView('settingsPageUsers', [
        'userTitle'             => $userTitle,
        'user'                  => $user,
        'controllersName'       => @$adminControllerName,
        'controllersEnabledName' => CvipHelper::getControllersName($this->adminControllersFiles),
        'metodos'               => $this->metodos
      ], 'admin');
    }
    else {
      $this->default();
    }
  }

  # ==================
  # ====== BETA ======
  # ==================

}
