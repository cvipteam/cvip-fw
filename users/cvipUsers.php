<?php
    add_action( 'show_user_profile', 'cvip_add_custom_user_fields' );
    add_action( 'edit_user_profile', 'cvip_add_custom_user_fields' );
    add_action( 'user_new_form', 'cvip_add_custom_user_fields' );

    # = GUARDAR DATOS =
    add_action( 'personal_options_update', 'cvip_save_custom_user_fields' );
    add_action( 'edit_user_profile_update', 'cvip_save_custom_user_fields' );

    # = Permisos =
    # = BLOQUEO ACCESO AL PERFIL =
    add_action('after_setup_theme','remove_admin_bar' );
    add_action( 'admin_menu', 'cvip_gl_capacidades' );

   # = SE AGREGAN LOS CAMPOS AL USUARIO =
   function cvip_add_custom_user_fields( $user ) {
    $datos['user']=$user;//Para poder consultar el id del usuario y su meta
    // echo controller::vista('datosUsuarios', $datos);
   }

   function cvip_save_custom_user_fields( $user_id ) {
     if ( !current_user_can( 'edit_user', $user_id ) ) {
         return false;
     }
     foreach ($_POST as $key => $value) {
           //Sanitize and validate the data
           update_user_meta( $user_id, "{$key}", sanitize_text_field($value) );
     }
   }

  # = PERMISOS =
  # = BLOQUEAR ACCESO AL PERFIL =
   function remove_admin_bar() {
   	if (!current_user_can('activate_plugins')) {
   		global $pagenow;
   		show_admin_bar(false);
   	}
   }

   function cvip_gl_capacidades(){
   	//MATA EL PROCESO SI NO ES ADMIN
   	if(!current_user_can('activate_plugins')) {
   		global $pagenow;
   		/*if ($pagenow == 'index.php' OR $pagenow == 'profile.php') {
   			wp_die( 'Por favor contacta a tu admin para solicitar acceso adicional.' );
      }*/
   	}
   }

   # = SE CREAN LOS ROLES (USUARIOS) AL ACTIVAR EL PLUGIN =
  function cvip_gl_add_role($appName, $adminControllersFiles) {

     $mainUser = strtoupper($appName);
     //SE AGREGA EL ROL AL PRINCIPAL
     add_role($mainUser, $mainUser, array('read' => true, 'read_private_pages' => true));
     //LE AGREGO EL ROL AL ADMIN
     $GLOBALS['wp_roles']->add_cap('administrator', $mainUser);
     //AGREGO EL ROL A TODOS LOS USUARIOS

     foreach ($adminControllersFiles as $user) {
       $user = CvipHelper::cleanString($user, "Controller");
       add_role($user, CvipHelper::spaceCaps($user), array(
         'read' => true,
         'read_private_pages' => true,
         $mainUser => true,
         'delete_posts' => true,
         'manage_own_attachments' => true,
         'edit_posts' => true,
         'upload_files' => true)
      );

       $GLOBALS['wp_roles']->add_cap('administrator', $user);
       $GLOBALS['wp_roles']->add_cap($mainUser, $user);
     }
  }

  # = FUNCTION QUE DEPENDIENDO LAS OPCIONES DEL PANEL DE AJUSTES, AGREGA O REMUEVE CAPACIDADES =
  function optionsAndCaps($appName, $controllersEnabled){
    $user = wp_get_current_user();
    $useRole = CvipHelper::getCurrentUserRole();
    foreach ($controllersEnabled as $role) {
      foreach ($controllersEnabled as $adminController) {
        if ( get_option("cvip-user-enabled-$role-{$adminController}") && !empty(get_option("cvip-user-enabled-$role-{$adminController}")) || array_key_exists($useRole, $user->allcaps) ){
          if (!array_key_exists($adminController, $user->allcaps))
            $GLOBALS['wp_roles']->add_cap($useRole, $adminController);
        }else if ( array_key_exists($adminController, $user->allcaps) && ($useRole != 'administrator' || $useRole != strtoupper($appName)) ){
          $GLOBALS['wp_roles']->remove_cap($useRole, $adminController);
        }
      }
    }
  }
  
   # = SE REMUEVEN LOS ROLES (USUARIOS) AL DESACTIVAR EL PLUGIN =
  function cvip_gl_remove_role($appName, $adminControllersFiles) {
     remove_role(strtoupper($appName));
     foreach ($adminControllersFiles as $user) {
       $user = CvipHelper::cleanString($user, "Controller");
       remove_role($user);
     }
  }
